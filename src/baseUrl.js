const ClientId = '955e45a79a4da01bcebafe2f7be8d6f1'

const baseUrl = {
    Lpa_kejahatan : `https://dors.sops.polri.go.id/api/v1/lp_a${ClientId}`,
    Lpb_kejahatan : "https://dors.sops.polri.go.id/api/v1/lp_b" + ClientId,
    Lp_gangguan : "https://dors.sops.polri.go.id/api/v1/lp_gangguan" + ClientId,
    Lp_pelanggaran : "https://dors.sops.polri.go.id/api/v1/lp_pelanggaran" + ClientId,
    Lp_bencana : "https://dors.sops.polri.go.id/api/v1/lp_bencana" + ClientId,
    serla : "https://dors.sops.polri.go.id/api/v1/serla" + ClientId,
    polda : "https://dors.sops.polri.go.id/api/v1/polda" + ClientId,
    polres : "https://dors.sops.polri.go.id/api/v1/polres" + ClientId,
    polsek : "https://dors.sops.polri.go.id/api/v1/polsek" + ClientId,
  };

  export default baseUrl;