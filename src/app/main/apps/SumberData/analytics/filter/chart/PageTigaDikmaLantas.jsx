import { AppBar, Card, Icon, IconButton, Tab, Tabs, Typography } from "@mui/material";
import React, { useState } from "react";

export const PageTigaDikmaLantas = () => {
  const [tabIndex, setTabIndex] = useState(0);
  return (
    <div>
      <Card className="w-full rounded-20 shadow">
        <AppBar position="static" elevation={0}>
          <div className="px-8 py-20 flex flex-row items-start justify-between">
            <div className="px-12">
              <Typography className="h3 font-medium mb-4">Total Kejahatan</Typography>
              <Typography className="h5" color="inherit">
                4.230 Kejahatan
              </Typography>
            </div>
            <div className="-mt-12">
              <IconButton aria-label="more" color="inherit" size="large">
                <Icon>more_vert</Icon>
              </IconButton>
            </div>
          </div>
        </AppBar>
      </Card>
    </div>
  );
};
