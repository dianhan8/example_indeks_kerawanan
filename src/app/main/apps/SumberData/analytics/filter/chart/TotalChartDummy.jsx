import React from "react";

import Chart from "react-apexcharts";

const TotalChartDummy = () => {
  const options = {
    xaxis: {
      categories: ["Total Suara"]
    //   categories: ["Anis Baswedan", "Ganjar Pranowo", "Prabowo Sugianto", "Erik Thohir", "Puan Maharani"]
    },
    plotOptions: {
      bar: {
          horizontal: false,
          borderRadius: 10,
          borderRadiusApplication: 'around',
          borderRadiusWhenStacked: 'last',
          columnWidth: '70%',
          barHeight: '70%',
          distributed: false,
          rangeBarOverlap: true,
          rangeBarGroupRows: false,
          colors: {
              ranges: [{
                  from: 0,
                  to: 0,
                  color: undefined
              }],
              backgroundBarColors: [],
              backgroundBarOpacity: 1,
              backgroundBarRadius: 0,
          },
          dataLabels: {
              position: 'top',
              maxItems: 100,
              hideOverflowingLabels: true,
              // orientation: horizontal,
              total: {
                enabled: false,
                formatter: undefined,
                offsetX: 0,
                offsetY: 0,
                style: {
                  color: '#373d3f',
                  fontSize: '12px',
                  fontFamily: undefined,
                  fontWeight: 600
                }
              }
          }
      }
  },
  dataLabels: {
    enabled: true,
    enabledOnSeries: undefined,
    formatter: function (val, opts) {
        return val
    },
    textAnchor: 'middle',
    distributed: false,
    offsetX: 0,
    offsetY: 0,
    style: {
        fontSize: '14px',
        fontFamily: 'Helvetica, Arial, sans-serif',
        fontWeight: 'bold',
        colors: undefined
    },
    background: {
      enabled: true,
      foreColor: '#fff',
      padding: 4,
      borderRadius: 2,
      borderWidth: 1,
      borderColor: '#fff',
      opacity: 0.9,
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: '#000',
        opacity: 0.45
      }
    },
    dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: '#000',
        opacity: 0.45
    }
  }
  };
  const series = [
    {
      name: "Anies Baswedan",
      data: [735]
    },
    {
      name: "Ganjar Pranowo",
      data: [668]
    },
    {
      name: "Prabowo Sugianto",
      data: [700]
    },
    {
      name: "Erik Thohir",
      data: [446]
    },
    {
      name: "Puan Maharani",
      data: [254]
    },
    
  ];

  return <Chart options={options} series={series} height={670} type="bar" />;
};
export default TotalChartDummy;
