import { lazy } from 'react';

const SumberDataApp = lazy(() => import('./SumberDataApp'));

const SumberDataAppConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'apps/SumberData/analytics',
      element: <SumberDataApp />,
    },
  ],
};

export default SumberDataAppConfig;
