import Avatar from '@mui/material/Avatar';
import { lighten } from '@mui/material/styles';
import Hidden from '@mui/material/Hidden';
import Icon from '@mui/material/Icon';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from '@lodash';
import { Box } from '@mui/system';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { getProjects, selectProjects } from '../project/store/projectsSlice';
import { selectWidgets } from '../project/store/widgetsSlice';

function SumberDataAppHeader(props) {
  const { pageLayout } = props;

  const dispatch = useDispatch();
  const widgets = useSelector(selectWidgets);
  const projects = useSelector(selectProjects);
  const user = useSelector(({ auth }) => auth.user);

  const [selectedProject, setSelectedProject] = useState({
    id: 1,
    menuEl: null,
  });

  useEffect(() => {
    dispatch(getProjects());
  }, [dispatch]);

  function handleChangeProject(id) {
    setSelectedProject({
      id,
      menuEl: null,
    });
  }

  function handleOpenProjectMenu(event) {
    setSelectedProject({
      id: selectedProject.id,
      menuEl: event.currentTarget,
    });
  }

  function handleCloseProjectMenu() {
    setSelectedProject({
      id: selectedProject.id,
      menuEl: null,
    });
  }

  if (_.isEmpty(projects)) {
    return null;
  }

  return (
    <Box
      sx={{
        background: (theme) => lighten(theme.palette.primary.dark, 0.1),
        color: (theme) => theme.palette.primary.contrastText,
        borderRadius: ' 0 0 0 0',
      }}
      // className="pb-110"
    >
      <div className="flex flex-col justify-between flex-1 min-w-0 px-8 pt-8">
        <div className="flex justify-between items-center">
          <div className="flex items-center min-w-0">
            {user.data.photoURLHeader ? (
              <FuseAnimate animation="transition.expandIn" delay={1000}>
                <Avatar
                  className="w-52 h-52 sm:w-64 sm:h-64"
                  alt="user photo"
                  src={user.data.photoURLHeader}
                />
              </FuseAnimate>
            ) : (
              <Avatar className="w-52 h-52 sm:w-64 sm:h-64">{user.data.displayName[0]}</Avatar>
            )}
            <div className="mx-12 min-w-0">
              <FuseAnimate animation="transition.slideLeftIn" delay={800}>
                <Typography variant="h3" className="text-25 sm:text-24 md:text-32 font-bold leading-none mb-8 tracking-tight">
                Sentiment Online
                </Typography>
              </FuseAnimate>
            </div>
          </div>
          <hr className="border-2 " />
          <Hidden lgUp>
            <IconButton
              onClick={(ev) => pageLayout.current.toggleRightSidebar()}
              aria-label="open left sidebar"
              color="inherit"
              size="large"
            >
              <Icon>menu</Icon>
            </IconButton>
          </Hidden>
        </div> 
      </div>
    </Box>
  );
}

export default SumberDataAppHeader;
