import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { FilterDataDasboard } from './FilterDataDasboard';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function DialogFilter() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button sx={{ m: 1, minWidth: 150 }} variant="outlined" onClick={handleClickOpen}>
        Cari Provinsi
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        onSave={handleSave}
        aria-describedby="alert-dialog-slide-description"
        maxWidth="xl"
      >
        {/* <DialogTitle>{"Cari Provinsi"}</DialogTitle> */}
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <FilterDataDasboard/>
          </DialogContentText>
        </DialogContent>
        <div className="flex justify-center gap-10">
        <DialogActions>
          <Button variant="contained" onClick={handleClose}>Batalkan</Button>
        </DialogActions>
        <DialogActions>
          <Button variant="contained" onClick={handleSave}>Simpan</Button>
        </DialogActions>
        </div>
      </Dialog>
    </div>
  );
}
