/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import Card from '@mui/material/Card';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import Box from '@mui/material/Box';
import { Button, Icon } from '@mui/material';
// import Icon from '@mui/material/Icon';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { ChartGlobalPrediktif } from './ChartGlobalPrediktif';

const dataKejahatan20Series = [
  2302, 334, 3930, 1340, 2800, 3200, 1200, 1440, 1003, 1230, 1200, 1420,
];
const dataKejahatan21Series = [
  1202, 1934, 1900, 1500, 1800, 1200, 1900, 1900, 1000, 1400, 1100, 1800,
];
const dataKejahatan22Series = [
  1400, 1300, 1200, 1100, 1500, 1600, 1200, 1200, 1700, 1400, 1700, 1100,
];
const dataKejahatan23Series = [
  1190, 1300, 2340, 1220, 1590, 1990, 1250, 1080, 2000, 2380, 2420, 2190,
];

const dataPenyelesaian20Series = [342, 331, 554, 635, 626, 232, 421, 178, 376, 243, 599, 645];
const dataPenyelesaian21Series = [343, 231, 454, 675, 676, 234, 121, 878, 676, 343, 899, 545];
const dataPenyelesaian22Series = [243, 131, 154, 235, 376, 634, 221, 778, 176, 843, 199, 545];

const dataDummy20 = {
  series: {
    2020: [
      {
        name: 'Jumlah Kejahatan',
        data: dataKejahatan20Series,
      },
      {
        name: 'Penyelesaian Perkara',
        data: dataPenyelesaian20Series,
      },
      // {
      //   name: 'Prediksi Kejahatan',
      //   data: [241, 541, 658, 875, 376, 174, 521, 278, 746, 263, 939, 155],
      // },
    ],
  },
};

// eslint-disable-next-line import/prefer-default-export
export const ChartPrediksi = () => {
  const datas = {
    id: 'widget5',
    series: {
      2022: [
        {
          name: 'Anies Baswedan',
          data: dataKejahatan21Series,
        },
        {
          name: 'Ganjar Pranowo',
          data: dataPenyelesaian21Series,
        },
        {
          name: 'Prabowo Sugianto',
          data: [143, 211, 352, 272, 126, 535, 822, 372, 123, 443, 841, 342],
        },
        {
          name: 'Erik Thohir',
          data: [113, 231, 554, 571, 246, 234, 171, 778, 676, 545, 391, 345],
        },
        {
          name: 'Puan Maharani',
          data: [243, 531, 354, 175, 676, 334, 721, 978, 276, 143, 799, 345],
        },
      ],
      2023: [
        {
          name: 'Anies Baswedan',
          data: dataKejahatan22Series,
        },
        {
          name: 'Ganjar Pranowo',
          data: dataPenyelesaian22Series,
        },
        {
          name: 'Prabowo Sugianto',
          data: [243, 231, 652, 275, 176, 135, 823, 272, 775, 243, 991, 142],
        },
        {
          name: 'Erik Thohir',
          data: [213, 631, 154, 575, 276, 634, 871, 278, 276, 443, 299, 445],
        },
        {
          name: 'Puan Maharani',
          data: [443, 331, 454, 575, 176, 234, 321, 478, 176, 743, 199, 145],
        },
      ],
      2024: [
        {
          name: 'Anies Baswedan',
          data: dataKejahatan22Series,
        },
        {
          name: 'Ganjar Pranowo',
          data: dataKejahatan23Series,
        },
        {
          name: 'Prabowo Sugianto',
          data: [543, 331, 752, 175, 276, 835, 223, 272, 175, 343, 191, 242],
        },
        {
          name: 'Erik Thohir',
          data: [813, 231, 654, 175, 576, 234, 371, 178, 876, 543, 199, 245],
        },
        {
          name: 'Puan Maharani',
          data: [143, 531, 254, 275, 676, 134, 421, 478, 176, 543, 199, 245],
        },
      ],
    },
    options: {
      chart: {
        type: 'area',
        height: '100%',
        stacked: true,
        foreColor: '#999',
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
      },
      stroke: {
        curve: 'smooth',
        width: 3,
      },
      dataLabels: {
        enabled: false,
      },
      markers: {
        size: 0,
        strokeColor: '#fff',
        strokeWidth: 3,
        strokeOpacity: 1,
        fillOpacity: 1,
        hover: {
          size: 6,
        },
      },
      xaxis: {
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'Mei',
          'Jun',
          'Juli',
          'Agu',
          'Sep',
          'Okt',
          'Nov',
          'Des',
        ],
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        tooltip: {
          enabled: true,
        },
      },
      grid: {
        position: 'back',
      },
      legend: {
        show: false,
      },
      fill: {
        type: 'solid',
        opacity: 0.7,
      },
      tooltip: {
        followCursor: true,
        theme: 'dark',
        fixed: {
          enabled: false,
          position: 'topRight',
          offsetX: 0,
          offsetY: 0,
        },
      },
    },
  };
  const theme = useTheme();
  const [tabValue, setTabValue] = useState(0);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const dataKejahatan20 = dataDummy20.series[2020].map((arr) => arr.data);
  const dataKejahatan21 = datas.series[2022].map((arr) => arr.data);
  const dataKejahatan22 = datas.series[2023].map((arr) => arr.data);
  const dataKejahatan23 = datas.series[2024].map((arr) => arr.data);

  const arrJumlahKejahatan20 = dataKejahatan20[0];
  const arrJumlahKejahatan21 = dataKejahatan21[0];
  const arrJumlahKejahatan22 = dataKejahatan22[0];
  const arrJumlahKejahatan23 = dataKejahatan23[0];
  const arrJumlahPenyelesaian20 = dataKejahatan21[1];
  const arrJumlahPenyelesaian21 = dataKejahatan21[1];
  const arrJumlahPenyelesaian22 = dataKejahatan22[1];
  const arrJumlahPenyelesaian23 = dataKejahatan23[1];
  // const arrJumlahPrediksi20 = dataKejahatan21[2];
  // const arrJumlahPrediksi21 = dataKejahatan21[2];
  // const arrJumlahPrediksi22 = dataKejahatan22[2];
  // const arrJumlahPrediksi23 = dataKejahatan23[2];

  const initialValue = 0;
  // data 2020
  const resultdataJumlahKejahatan20 = arrJumlahKejahatan20?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  const resultdataJumlahPenyelesaian20 = arrJumlahPenyelesaian20?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  // const resultdataJumlahPrediksi20 = arrJumlahPrediksi20?.reduce(
  //   (accumulator, currentValue) => accumulator + currentValue,
  //   initialValue
  // );
  // end data 2020
  // data 2021
  const resultdataJumlahKejahatan21 = arrJumlahKejahatan21?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  const resultdataJumlahPenyelesaian21 = arrJumlahPenyelesaian21?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  // const resultdataJumlahPrediksi21 = arrJumlahPrediksi21?.reduce(
  //   (accumulator, currentValue) => accumulator + currentValue,
  //   initialValue
  // );
  // data 2022
  const resultdataJumlahKejahatan22 = arrJumlahKejahatan22?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  const resultdataJumlahPenyelesaian22 = arrJumlahPenyelesaian22?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  // const resultdataJumlahPrediksi22 = arrJumlahPrediksi22?.reduce(
  //   (accumulator, currentValue) => accumulator + currentValue,
  //   initialValue
  // );
  // data 2023
  const resultdataJumlahKejahatan23 = arrJumlahKejahatan23?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  const resultdataJumlahPenyelesaian23 = arrJumlahPenyelesaian23?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  // const resultdataJumlahPrediksi23 = arrJumlahPrediksi23?.reduce(
  //   (accumulator, currentValue) => accumulator + currentValue,
  //   initialValue
  // );

  const MeanKejahatan = [];
  MeanKejahatan.push(
    resultdataJumlahKejahatan21,
    resultdataJumlahKejahatan22,
    resultdataJumlahKejahatan23,
    resultdataJumlahKejahatan20
  );
  const resultSumKejahatan = MeanKejahatan?.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    initialValue
  );
  const resultMeanKejahatan = resultSumKejahatan / MeanKejahatan.length;
  // console.log(resultMeanKejahatan, 'nilai Rata2 kejahatan');

  // presentase Kejahatan perTahun 21
  const Presentase21 = resultdataJumlahKejahatan20 - resultdataJumlahKejahatan21;
  const result21 = Math.round((Presentase21 / resultdataJumlahKejahatan21) * 100);
  // console.log(result21, 'persen21');

  // end presentase Kejahatan perTahun
  // presentase Kejahatan perTahun 22
  const Presentase22 = resultdataJumlahKejahatan21 - resultdataJumlahKejahatan22;
  const result22 = Math.round((Presentase22 / resultdataJumlahKejahatan22) * 100);
  // console.log(result22, 'persen22');

  // end presentase Kejahatan perTahun
  // presentase Kejahatan perTahun 23
  const Presentase23 = resultdataJumlahKejahatan22 - resultdataJumlahKejahatan23;
  const result23 = Math.round((Presentase23 / resultdataJumlahKejahatan23) * 100);
  // console.log(result23, 'persen23');

  // end presentase Kejahatan perTahun
  const series = datas.series[Object.keys(datas.series)[tabValue]];
  // eslint-disable-next-line no-undef
  _.setWith(datas, 'options.colors', [theme.palette.secondary.main, theme.palette.primary.main]);

  // dataPrediktif

  return (
    <div>
      <Card className="w-full rounded-20 shadow">
        <div className="relative p-20 flex flex-row items-center justify-between">
          <div className="flex flex-col">
            <Typography className="h2 sm:h2 font-bold">
              Data Predictive Capres 2024
            </Typography>
            {tabValue === 0 ? (
              <FuseAnimate key={0} animation="transition.slideLeftIn" delay={300}>
                <div className="flex flex-wrap">
                  {result21 > 0 && (
                    <Icon fontSize="medium" className="text-green text-20 m-5">
                      trending_up
                    </Icon>
                  )}
                  {result21 < 0 && <Icon className="text-red text-20 m-5">trending_down</Icon>}
                  <Typography className="font-semibold  m-5" color="textSecondary">
                    {/* {data.visits.ofTarget}% */}
                    {result21}%
                  </Typography>
                  <Typography className="whitespace-nowrap mx-4 mt-5" color="textSecondary">
                    {result21 > 0 && 'Meningkat'}
                    {result21 < 0 && 'Menurun'}
                  </Typography>
                </div>
              </FuseAnimate>
            ) : tabValue === 1 ? (
              <FuseAnimate key={1} animation="transition.slideLeftIn" delay={300}>
                <div className="flex flex-wrap">
                  {result22 > 0 && (
                    <Icon fontSize="medium" className="text-green text-20 m-5">
                      trending_up
                    </Icon>
                  )}
                  {result22 < 0 && <Icon className="text-red text-20 m-5">trending_down</Icon>}
                  <Typography className="font-semibold  m-5" color="textSecondary">
                    {/* {data.visits.ofTarget}% */}
                    {result22}%
                  </Typography>
                  <Typography className="whitespace-nowrap mx-4 mt-5" color="textSecondary">
                    {result22 > 0 && 'Meningkat'}
                    {result22 < 0 && 'Menurun'}
                  </Typography>
                </div>
              </FuseAnimate>
            ) : (
              <FuseAnimate key={2} animation="transition.slideLeftIn" delay={300}>
                <div className="flex flex-wrap">
                  {result23 > 0 && (
                    <Icon fontSize="medium" className="text-green text-20 m-5">
                      trending_up
                    </Icon>
                  )}
                  {result23 < 0 && <Icon className="text-red text-20 m-5">trending_down</Icon>}
                  <Typography className="font-semibold  m-5" color="textSecondary">
                    {/* {data.visits.ofTarget}% */}
                    {result23}%
                  </Typography>
                  <Typography className="whitespace-nowrap mx-4 mt-5" color="textSecondary">
                    {result23 > 0 && 'Meningkat'}
                    {result23 < 0 && 'Menurun'}
                  </Typography>
                </div>
              </FuseAnimate>
            )}
          </div>
          <div className="flex flex-row items-center">
            <Button className="" variant="contained" onClick={handleClickOpen}>
            Predictive
            </Button>
            <div>
              <Dialog
                // maxWidth='sm'
                // fullWidth
                fullScreen
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">Global Data Predictive</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    <ChartGlobalPrediktif
                      resultdataJumlahKejahatan21={resultdataJumlahKejahatan21}
                      resultdataJumlahKejahatan22={resultdataJumlahKejahatan22}
                      resultdataJumlahKejahatan23={resultdataJumlahKejahatan23}
                      resultdataJumlahPenyelesaian21={resultdataJumlahPenyelesaian21}
                      resultdataJumlahPenyelesaian22={resultdataJumlahPenyelesaian22}
                      resultdataJumlahPenyelesaian23={resultdataJumlahPenyelesaian23}
                    />
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button variant="contained" onClick={handleClose}>
                    Tutup
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
            <Tabs
              value={tabValue}
              onChange={(event, value) => setTabValue(value)}
              indicatorColor="secondary"
              textColor="inherit"
              variant="scrollable"
              scrollButtons={false}
              className="w-full px-24 -mx-4 min-h-40"
              classes={{ indicator: 'flex justify-center bg-transparent w-full h-full' }}
              TabIndicatorProps={{
                children: (
                  <Box
                    sx={{ bgcolor: 'text.disabled' }}
                    className="w-full h-full rounded-full opacity-20"
                  />
                ),
              }}
            >
              {Object.keys(datas.series).map((key) => (
                <Tab
                  key={key}
                  className="text-14 font-semibold min-h-40 min-w-64 mx-4 px-12 capitalize"
                  disableRipple
                  label={key}
                />
              ))}
            </Tabs>
          </div>
        </div>

        <div className="relative h-200 sm:h-320 sm:pb-16">
          <ReactApexChart
            options={datas.options}
            series={series}
            type={datas.options.chart.type}
            height={datas.options.chart.height}
          />
        </div>
      </Card>
    </div>
  );
};
