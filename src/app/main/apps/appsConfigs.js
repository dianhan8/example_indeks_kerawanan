import AnalyticsDashboardAppConfig from './dashboards/analytics/AnalyticsDashboardAppConfig';
import ProjectDashboardAppConfig from './dashboards/project/ProjectDashboardAppConfig';
// import Covid19Config from './covid19/Covid19Config';
import ProfilingDashboardAppConfig from './ProfilingOrmas/analytics/ProfilingDashboardAppConfig';
import ReportAppConfig from './Report/analytics/ReportAppConfig';
import AnalitikAppConfig from './Analitik/analytics/AnalitikAppConfig';
import SumberDataAppConfig from './SumberData/analytics/SumberDataAppConfig';
import UserManagementAppConfig from './UserManagement/analytics/UserManagementAppConfig';

const appsConfigs = [
  AnalyticsDashboardAppConfig,
  ProfilingDashboardAppConfig,
  ReportAppConfig,
  AnalitikAppConfig,
  SumberDataAppConfig,
  UserManagementAppConfig,
  ProjectDashboardAppConfig,
  // Covid19Config,
];

export default appsConfigs;
