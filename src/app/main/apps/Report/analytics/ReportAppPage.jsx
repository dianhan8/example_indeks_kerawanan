import FusePageSimple from '@fuse/core/FusePageSimple';
import { styled } from '@mui/material/styles';
// import withReducer from 'app/store/withReducer';

import _ from '@lodash';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseAnimate from '@fuse/core/FuseAnimate';
import reducer from './store';
// import { getWidgets, selectWidgets } from './store/widgetsSlice';
import { selectWidgets } from '../project/store/widgetsSlice';
import { getWidgets } from './store/widgetsSlice';
import ReportAppHeader from './ReportAppHeader';
import ReportApp from './ReportApp';
import PeriodesasiData from './componentAssets/PeriodesasiData';
import DialogFilter from './filter/DialogFilter';

const Root = styled(FusePageSimple)(({ theme }) => ({
  '& .FusePageSimple-header': {
    minHeight: 100,
    height: 100,
    [theme.breakpoints.up('lg')]: {
      marginRight: 12,
      borderBottomRightRadius: 20,
    },
  },
  '& .FusePageSimple-toolbar': {
    minHeight: 56,
    height: 56,
    alignItems: 'flex-end',
  },
  '& .FusePageSimple-rightSidebar': {
    width: 288,
    border: 0,
    padding: '12px 0',
  },
  '& .FusePageSimple-content': {
    maxHeight: '100%',
    '& canvas': {
      maxHeight: '100%',
    },
  },
}));

function ReportAppPage(props) {
  const dispatch = useDispatch();
  const widgets = useSelector(selectWidgets);

  const pageLayout = useRef(null);
  const [tabValue, setTabValue] = useState(0);

  useEffect(() => {
    dispatch(getWidgets());
  }, [dispatch]);

  function handleChangeTab(event, value) {
    setTabValue(value);
  }

  if (_.isEmpty(widgets)) {
    return null;
  }

  return (
    <Root
      header={<ReportAppHeader pageLayout={pageLayout} />}
      contentToolbar={
        <div>
          <FuseAnimate animation="transition.expandIn" delay={3000}>
            <div className="flex justify-start">
              <div>
                <PeriodesasiData />
              </div>
              <div>
                <DialogFilter />
              </div>
            </div>
          </FuseAnimate>
        </div>
      }
      content={
        <div className="p-12 lg:ltr:pr-0 lg:rtl:pl-0">
          <ReportApp />
        </div>
      }
      // rightSidebarContent={<ProjectDashboardAppSidebar />}
      ref={pageLayout}
    />
  );
}

export default withReducer('ReportApp', reducer)(ReportAppPage);
