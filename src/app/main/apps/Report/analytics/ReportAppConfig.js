import { lazy } from 'react';

const ReportApp = lazy(() => import('./ReportApp'));

const ReportAppConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'apps/Report/analytics',
      element: <ReportApp />,
    },
  ],
};

export default ReportAppConfig;
