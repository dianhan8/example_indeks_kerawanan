import { Paper } from "@mui/material";
import React, { useState } from "react";
import Chart from "react-apexcharts";

export const JumlahSelesaiPolri = (props) => {
  const [grafik, setGrafik] = useState({
    chart: {
      id: "apexcharts-example",
    },
    xaxis: {
      categories: [
        "jan",
        "Feb",
        "Mar",
        "Apr",
        "Mei",
        "Jun",
        "Jul",
        "Agust",
        "Sept",
        "Nov",
        "Des",
      ],
    },
    responsive: [
      {
        breakpoint: 1000,
        options: {
          plotOptions: {
            bar: {
              horizontal: true,
              width : 300,
              height: 300
            }
          },
          legend: {
            position: "bottom",
            width : 300
          }
        }
      }
    ]
  });
  const [series, setSeries] = useState([
    {
      name: "Data Normal",
      data: [80, 75, 63, 85, 50, 60, 110, 150, 177, 202, 335],
    },
  ]);
  return (
    <React.Fragment>
      <div className="p-5 m-5">
        <Paper elevation={2} className="p-5">
          <Chart
            options={grafik}
            series={series}
            type="line"
            className="md:w-32 lg:w-800"
            // width={800}
            height={400}
          />
        </Paper>
      </div>
    </React.Fragment>
  );
};
