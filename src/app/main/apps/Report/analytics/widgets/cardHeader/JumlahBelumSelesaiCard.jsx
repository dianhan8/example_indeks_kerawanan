import Card from '@mui/material/Card';
import Icon from '@mui/material/Icon';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import _ from '@lodash';
import { JumlahBelumSelesaiPolri } from '../../grafik/grafikCard/JumlahBelumSelesaiPolri';

function JumlahBelumSelesaiCard(props) {
  const theme = useTheme();
  const data = _.merge({}, props.data);
  const [open, setOpen] = useState(false);

  _.setWith(data, 'options.colors', [theme.palette.error.main]);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card className="w-full rounded-20 shadow">
    <div className="p-2 pb-0">
     <Typography className="h2 font-medium text-bold text-center">Jumlah Penduduk</Typography>
        {/* <Typography className="h3 font-medium">Politik</Typography> */}

        <div className="p-2 pb-0">
        <Typography className="text-32 font-medium text-bold text-center">
            {data.visits3.value}
          </Typography>
        </div>
      </div>
      <div className="flex justify-center mb-10">
        <Button
          className="bg-grey-200 flex justify-center pt-2"
          onClick={handleClickOpen}
        >
          Tampilkan Grafik
        </Button>
        <Dialog
          open={open}
          onClose={handleClickOpen}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="md"
        >
          <DialogTitle id="alert-dialog-title">
            Grafik Jumlah Penduduk
          </DialogTitle>
          <DialogContent>
          <div >
            <JumlahBelumSelesaiPolri />
          </div>
          </DialogContent>
          <DialogActions>
            <Button className="bg-grey-200" onClick={handleClose} autoFocus>
              Kembali
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {/* <div className="h-110 w-100-p">
        <ReactApexChart
          options={data.options}
          series={data.series}
          type={data.options.chart.type}
          height={data.options.chart.height}
        />
      </div> */}
    </Card>
  );
}

export default memo(JumlahBelumSelesaiCard);
