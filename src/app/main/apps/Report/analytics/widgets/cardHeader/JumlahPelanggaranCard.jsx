import Card from '@mui/material/Card';
import Icon from '@mui/material/Icon';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
import _ from '@lodash';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { JumlahPelanggaran } from '../../grafik/grafikCard/JumlahPelanggaran';

function JumlahPelanggaranCard(props) {
  const theme = useTheme();
  const data = _.merge({}, props.data);
  const [open, setOpen] = useState(false);

  _.setWith(data, 'options.colors', [theme.palette.secondary.main]);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card className="w-full rounded-20 shadow">
      <div className="p-20 pb-0">
        <Typography className="h3 font-medium">Isseu Media </Typography>
        {/* <Typography className="h3 font-medium">Counter Sentiment</Typography> */}

        <div className="flex flex-row flex-wrap items-center mt-12">
          <Typography className="text-48 font-semibold leading-none tracking-tighter">
            {data.impressions.value}
          </Typography>

          <div className="flex flex-col mx-8">
            {data.impressions.ofTarget > 0 && (
              <Icon className="text-green text-20">trending_up</Icon>
            )}
            {data.impressions.ofTarget < 0 && (
              <Icon className="text-red text-20">trending_down</Icon>
            )}
            <div className="flex items-center">
              <Typography className="font-semibold" color="textSecondary">
                {data.impressions.ofTarget}%
              </Typography>
              <Typography className="whitespace-nowrap mx-4" color="textSecondary">
                of target
              </Typography>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-center mb-10">
        <Button className="bg-grey-200 flex justify-center pt-2" onClick={handleClickOpen}>
          Tampilkan Grafik
        </Button>
        <Dialog
          open={open}
          onClose={handleClickOpen}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="md"
        >
          <DialogTitle id="alert-dialog-title">Grafik Jumlah Pelanggaran</DialogTitle>
          <DialogContent>
            <div>
              <JumlahPelanggaran />
            </div>
          </DialogContent>
          <DialogActions>
            <Button className="bg-grey-200" onClick={handleClose} autoFocus>
              Kembali
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {/* <div className="h-110 w-100-p">
        <ReactApexChart
          options={data.options}
          series={data.series}
          type={data.options.chart.type}
          height={data.options.chart.height}
        />
      </div> */}
    </Card>
  );
}

export default memo(JumlahPelanggaranCard);
