import { lazy } from 'react';

const AnalitikApp = lazy(() => import('./AnalitikApp'));

const AnalitikAppConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'apps/Analitik/analytics',
      element: <AnalitikApp />,
    },
  ],
};

export default AnalitikAppConfig;
