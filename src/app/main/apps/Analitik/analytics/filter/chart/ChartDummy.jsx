import React from "react";

import Chart from "react-apexcharts";

const ChartDummy = () => {
  const options = {
    xaxis: {
      range: 10,
      categories: ["Partai Politik", "Suara Raykat"]
    //   categories: ["Anis Baswedan", "Ganjar Pranowo", "Prabowo Sugianto", "Erik Thohir", "Puan Maharani"]
    },
    plotOptions: {
      bar: {
          horizontal: false,
          borderRadius: 10,
          borderRadiusApplication: 'around',
          borderRadiusWhenStacked: 'last',
          columnWidth: '70%',
          barHeight: '70%',
          distributed: false,
          rangeBarOverlap: true,
          rangeBarGroupRows: false,
          colors: {
              ranges: [{
                  from: 0,
                  to: 0,
                  color: undefined
              }],
              backgroundBarColors: [],
              backgroundBarOpacity: 1,
              backgroundBarRadius: 0,
          },
          dataLabels: {
              position: 'top',
              maxItems: 100,
              hideOverflowingLabels: true,
              // orientation: horizontal,
              total: {
                enabled: false,
                formatter: undefined,
                offsetX: 0,
                offsetY: 0,
                style: {
                  color: '#373d3f',
                  fontSize: '12px',
                  fontFamily: undefined,
                  fontWeight: 600
                }
              }
          }
      }
  }
  };
  const series = [
    {
      name: "Anies Baswedan",
      data: [312, 423]
    },
    {
      name: "Ganjar Pranowo",
      data: [270, 398]
    },
    {
      name: "Prabowo Sugianto",
      data: [295, 405]
    },
    {
      name: "Erik Thohir",
      data: [189, 257]
    },
    {
      name: "Puan Maharani",
      data: [132, 122]
    }
  ];

  return <Chart options={options} series={series} height={700} type="bar" />;
};
export default ChartDummy;
