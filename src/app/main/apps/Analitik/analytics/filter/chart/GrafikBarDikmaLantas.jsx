import { Paper, Typography } from "@mui/material";
import React, { useState } from "react";
import ReactApexChart from "react-apexcharts";

export const GrafikBarDikmaLantas = () => {
    const [grafik, setGrafik] = useState({
        chart: {
          type: "area",
          height: "100%",
          stacked: true,
          foreColor: "#999",
          toolbar: {
            show: false,
          },
          zoom: {
            enabled: false,
          },
        },
        stroke: {
          curve: "smooth",
          width: 3,
        },
        dataLabels: {
            enabled: false,
            offsetX: -6,
            style: {
              fontSize: '12px',
              colors: ['#fff']
            }
        },
        plotOptions: {
            bar: {
              horizontal: true,
              dataLabels: {
                position: 'top',
              },
            }
          },
        markers: {
          size: 0,
          strokeColor: "#fff",
          strokeWidth: 3,
          strokeOpacity: 1,
          fillOpacity: 1,
          hover: {
            size: 6,
          },
        },
        xaxis: {
          categories: [
            "jan",
            "Feb",
            "Mar",
            "Apr",
            "Mei",
            "Jun",
            "Jul",
            "Agust",
            "Sept",
            "Nov",
            "Des",
          ],
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false,
          },
        },
        yaxis: {
          tooltip: {
            enabled: true,
          },
        },
        grid: {
          position: "back",
        },
        legend: {
          show: false,
        },
        fill: {
          type: "solid",
          opacity: 0.7,
        },
        tooltip: {
          followCursor: true,
          theme: "dark",
          fixed: {
            enabled: false,
            position: "topRight",
            offsetX: 0,
            offsetY: 0,
          },
        },
        responsive: [
          {
            breakpoint: 1000,
            options: {
              plotOptions: {
                bar: {
                  horizontal: true,
                  width: 300,
                  height: 300,
                },
              },
              legend: {
                position: "bottom",
                width: 300,
              },
            },
          },
        ],
      });
      const [series, setSeries] = useState([
        {
          name: "Kasus",
          data: [30, 40, 35, 50, 49, 60, 70, 91, 125, 202, 205],
        },
        {
          name: "Penyelesaian",
          data: [20, 45, 95, 30, 44, 70, 80, 93, 25, 20, 255],
        },
      ]);
  return (
    <div>
      <div className="ml-5 mr-5 h-full">
        <Paper elevation={1} className="pr-5 w-full h-full">
          <Typography className="m-14 font-semibold" variant="subtitle2">
            Jumlah Kejadian Menonjol
          </Typography>
          <hr className="ml-5 mr-5 border-1 border-inherit"></hr>
          <ReactApexChart
            options={grafik}
            series={series}
            type="bar"
            // width={800}
            height={400}
          />
        </Paper>
      </div>
    </div>
  );
};
