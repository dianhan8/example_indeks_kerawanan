import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useState } from "react";
import SearchIcon from '@mui/icons-material/Search';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';

export default function PeriodesasiData() {
  const [periodesasiData, setPeriodesasiData] = useState("");

  const handleChangePeriodesasiData = (event) => {
    setPeriodesasiData(event.target.value);
  };

  return (
    <div>
      <FormControl variant="outlined" size="small" sx={{ m: 1, minWidth: 200 }}>
       <Autocomplete
        freeSolo
        id="free-solo-2-demo"
        disableClearable
        // options={top100Films.map((option) => option.title)}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Input Propinsi Dashboard "
            InputProps={{
              ...params.InputProps,
              type: 'search', 
            }}
          />
        )}
      />
     
      </FormControl>
    </div>
  );
}
