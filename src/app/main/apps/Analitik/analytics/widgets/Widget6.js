import _ from '@lodash';
import Card from '@mui/material/Card';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
// import ReactApexChart from 'react-apexcharts';
import Box from '@mui/material/Box';
import Chart from 'react-apexcharts';


function Widget6() 

{
  const [state, setState] = useState({
      options: {
          labels: ['Positive', 'Neutral', 'Negative' ]
      },
      series : [48.8, 12.3, 39.9]

  });
    

  return (
    <Card className="w-full rounded-20 shadow">
       
        <div className="bar">
        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
            Perbandingan Kegiatan Meresahkan
            </Typography>
        </div>
        <Chart options={state.options} series={state.series} type="donut" width="425"/>
        </div>
        </Card>      
   
  );
}

export default memo(Widget6);

