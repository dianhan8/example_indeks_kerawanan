// import { Box, Menu, MenuItem, Button } from "@mui/material";
// import { useState } from "react";
// import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
// import filter from "lodash/filter";
// import get from "lodash/get";

// interface Item {
//   label: string;
//   value: string | any;
// }

// interface Props {
//   value: any;
//   placeholder?: string;
//   options: Item[];
//   onChange: (event: any, value: any) => void;
// }

// export default function CustomOptions(props: Props) {
//   const { options, value, onChange, placeholder = "Select" } = props;
//   const [placement, setPlacement] = useState(null);

//   const handleClose = () => {
//     setPlacement(null);
//   };

//   return (
//     <Box>
//       <Button
//         variant="text"
//         onClick={(event: any) => setPlacement(event.currentTarget)}
//         color="inherit"
//         endIcon={<KeyboardArrowDownIcon />}
//       >
//         {get(filter(options, { value }), [0, "label"], placeholder)}
//       </Button>

//       <Menu
//         anchorEl={placement}
//         open={Boolean(placement)}
//         onClose={handleClose}
//         PaperProps={{
//           style: {
//             maxHeight: 170,
//             minWidth: 100,
//           }
//         }}
//       >
//         {options.map((item: any, i: number) => {
//           const isActive = item.value === value;

//           return (
//             <MenuItem
//               value={item.value}
//               key={i}
//               dense
//               sx={(theme) => ({
//                 fontWeight: 700,
//                 color: isActive ? theme.palette.primary.main : "black",
//               })}
//               onClick={(event: any) => { onChange(event, item.value); handleClose();}}
//             >
//               {item.label}
//             </MenuItem>
//           );
//         })}
//       </Menu>
//     </Box>
//   );
// }
