/* eslint-disable func-names */
/* eslint-disable react/no-this-in-sfc */
import { useState } from 'react';
import { MapContainer, GeoJSON, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import './CovidMap.css';

const points = [
  {
    lat: 130.8313,
    lng: -3.47141,
    title: 'point 1',
  },
  {
    lat: 52.258071,
    lng: 20.986805,
    title: 'point 2',
  },
  {
    lat: 52.242728,
    lng: 21.041565,
    title: 'point 3',
  },
  {
    lat: 52.234213,
    lng: 21.029034,
    title: 'point 4',
  },
  {
    lat: 52.251661,
    lng: 21.003456,
    title: 'point 5',
  },
];

const CovidMap = ({ countries }) => {
  const mapStyle = {
    fillColor: 'white',
    weight: 1,
    color: 'black',
    fillOpacity: 1,
  };
  // marker porvinsi
  function getProvinsi(feature, layer) {
    if (feature.properties && feature.properties.Death_cases) {
      layer.bindPopup(feature.properties.Death_cases);
    }
  }

  const onEachCountry = (prov, layer) => {
    layer.options.fillColor = prov.properties.color;
    const confirmedText = prov.properties.Confirmed_cases;
    const name = prov.properties.Propinsi;
    const jumlahKasus = 'Jumlah Ormas:';
    const kurungbuka = '(';
    const kurungtutup = ')';
    layer.on('mouseover', function (e) {
      // getColor(prov)
      console.log(name, 'dapet');
      this.openPopup();
      // getProvinsi(prov, layer);

      // style
      this.setStyle({
        fillColor: '#eb4034',
        weight: 2,
        color: '#eb4034',
        fillOpacity: 0.7,
      });
      layer
        .bindTooltip(`${kurungbuka}${name}${kurungtutup} ${jumlahKasus} ${confirmedText}`, {
          // direction: 'right',
          permanent: false,
          sticky: true,
          offset: [10, 0],
          // opacity: 0.75,
          className: 'leaflet-tooltip-own',
        })
        .openTooltip();
    });
    layer.on('mouseout', function (e) {
      // this.closePopup();
      this.setStyle({
        fillColor: '#3388ff',
        weight: 2,
        color: '#3388ff',
        fillOpacity: 0.2,
      });
    });
  };
  const center = [-2.5489, 118.0149];
  const zoom = 4.5;
  const [map, setMap] = useState(null);

  return (
    <MapContainer
      style={{ height: '80vh', width: '100%' }}
      zoom={zoom}
      center={center}
      whenCreated={setMap}
      scrollWheelZoom={false}
      attributionControl={false}
    >
      <TileLayer
        url="https://api.maptiler.com/maps/basic/256/{z}/{x}/{y}.png?key=4s7rfOSa6Vy2uJarkM8k"
        // attribution='<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
      />
      <GeoJSON
        // style={mapStyle}
        data={countries}
        onEachFeature={onEachCountry}
      />
      {/* <Legends map={map}/> */}
      {/* <MyMarkers data={points}/> */}
    </MapContainer>
  );
};

export default CovidMap;
