import papa from "papaparse";
import legendItems from "../entities/LegendItems";
// import { features } from "../data/countries.json";
import features from "../data/Provinsi.json"
// import { provinsi } from "../data/provinsi";
// import { features } from "../data/datas.json";
//    this.setState(features);

class LoadCountryTask {
  covidUrl =
    // "https://github.com/benangmerah/wilayah/blob/master/datasources/daftar-nama-daerah.csv";
    "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/web-data/data/cases_country.csv";

  setState = null;

  load = (setState) => {
    this.setState = setState;

    papa.parse(this.covidUrl, {
      download: true,
      header: true,
      complete: (result) => this.#processCovidData(result.data),
    });
    // papa.parse(this.covidUrl, {
    //   download: true,
    //   header: true,
    //   complete: (result) => this.#processCovidData(result.data),
    // });
  };

  #processCovidData = (covidCountries) => {
    // console.log(covidCountries, "datas");
    for (let i = 0; i < features.length; i++) {
      const country = features[i];
      //console.log(country);
      const covidCountry = covidCountries.find(
        (covidCountry) => country.properties.ISO_A3 === covidCountry.ISO3
      );

      country.properties.confirmed = 0;
      country.properties.confirmedText = 0;

      if (covidCountry != null) {
        let confirmed = Number(covidCountry.Confirmed);
        country.properties.confirmed = confirmed;
        country.properties.confirmedText =
          this.#formatNumberWithCommas(confirmed);
      }
      this.#setCountryColor(country);
    }

    this.setState(features);
    console.log(features);
  };

  #setCountryColor = (country) => {
    const legendItem = legendItems.find((item) =>
      item.isFor(country.properties.confirmed)
    );

    if (legendItem != null) country.properties.color = legendItem.color;
  };

  #formatNumberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
}

export default LoadCountryTask;
