/* eslint-disable import/prefer-default-export */
import { AppBar, Card, Icon, IconButton, Typography } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react';

export const FakeCard = (props) => {
  const [card, setCard] = useState();
  useEffect(() => {
    axios
      .get(`http://103.140.90.122:3080/dashboard/card`)
      .then((res) => {
        console.log(res, 'berhasil penahanan Kota');
        setCard(res?.data);
      })
      .catch((err) => {
        console.log(err, 'errdummyfilterPenahanan Kota');
      });
  }, []);
  return (
    <div>
      {card?.map((item) => (
        <div className="flex justify-evenly gap-2 flex-col md:flex-row">
          <div className="basis-19 ">
            <Card className="w-full rounded-20 shadow ">
              <AppBar position="static" elevation={0}>
                <div className="px-8 py-20 flex flex-row items-start justify-between">
                  <div className="px-12">
                    <Typography className="h3 font-medium mb-4">{item?.keterangan}</Typography>
                    <Typography className="h5" color="inherit">
                      {item?.jumlah} Anggota
                    </Typography>
                  </div>
                  <div className="-mt-12">
                    <IconButton aria-label="more" color="inherit" size="large">
                      <Icon>more_vert</Icon>
                    </IconButton>
                  </div>
                </div>
              </AppBar>
            </Card>
          </div>
        </div>
      ))}
    </div>
  );
};
