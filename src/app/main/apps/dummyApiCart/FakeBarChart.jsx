/* eslint-disable array-callback-return */
/* eslint-disable import/prefer-default-export */
import { Paper, Typography } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';

export default function FakeBarChart() {
  const [barPolda, setbarPolda] = useState();
  useEffect(() => {
    axios
      .get(`http://103.140.90.122:3080/dashboard/bar/Polda`)
      .then((res) => {
        setbarPolda(res.data);
        console.log(res.data, 'fakeBar');
      })
      .catch((err) => {
        console.log(err, 'errbarPolda');
      });
  }, []);
  // console.log(barPolda, 'props barPolda');
  const resultPoldaOptions = [];
  const resultPoldaSeries = [];
  const result = [];
  barPolda?.map((item) => {
    // console.log(item.jumlah, 'FAKEitembar');
    const index = resultPoldaOptions.findIndex((element) => element === item.keterangan);
    if (index !== -1) {
      resultPoldaSeries[index] = resultPoldaSeries[index] ? resultPoldaSeries[index] + 1 : 1;
    } else {
      resultPoldaSeries[resultPoldaOptions?.length] = resultPoldaSeries[resultPoldaOptions?.length]
        ? resultPoldaSeries[resultPoldaOptions?.length] + 1
        : 1;
      resultPoldaOptions.push(item.keterangan);
    }
    const indexs = result.findIndex((element) => element === item.jumlah);
    if (indexs !== -1) {
      resultPoldaSeries[indexs] = resultPoldaSeries[indexs] ? resultPoldaSeries[indexs] + 1 : 1;
    } else {
      resultPoldaSeries[result?.length] = resultPoldaSeries[result?.length]
        ? resultPoldaSeries[result?.length] + 1
        : 1;
      result.push(item.jumlah);
    }
  });
  // console.log(resultPoldaOptions, 'Fake resultPoldaOptions');
  // console.log(resultPoldaSeries, 'fake resultPoldaSeries');
  // console.log(result, 'fake result');

  const grafik = {
    chart: {
      type: 'area',
      height: '100%',
      stacked: true,
      foreColor: '#999',
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    stroke: {
      curve: 'smooth',
      width: 3,
    },
    dataLabels: {
      enabled: false,
      offsetX: -6,
      style: {
        fontSize: '12px',
        colors: ['#fff'],
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        dataLabels: {
          position: 'top',
        },
      },
    },
    markers: {
      size: 0,
      strokeColor: '#fff',
      strokeWidth: 3,
      strokeOpacity: 1,
      fillOpacity: 1,
      hover: {
        size: 6,
      },
    },
    xaxis: {
      categories: resultPoldaOptions === undefined || '' ? [] : resultPoldaOptions,
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      tooltip: {
        enabled: true,
      },
    },
    grid: {
      position: 'back',
    },
    legend: {
      show: false,
    },
    fill: {
      type: 'solid',
      opacity: 0.7,
    },
    tooltip: {
      followCursor: true,
      theme: 'dark',
      fixed: {
        enabled: false,
        position: 'topRight',
        offsetX: 0,
        offsetY: 0,
      },
    },
    noData: {
      text: 'Data Tidak Ada Dalam DataBase',
      align: 'center',
      verticalAlign: 'middle',
      offsetX: 0,
      offsetY: 0,
      style: {
        color: undefined,
        fontSize: '14px',
        fontFamily: undefined,
      },
    },
    responsive: [
      {
        breakpoint: 1000,
        options: {
          plotOptions: {
            bar: {
              horizontal: true,
              width: 300,
              height: 300,
            },
          },
          legend: {
            position: 'bottom',
            width: 300,
          },
        },
      },
    ],
  };
  const series = [
    {
      name: 'Kasus',
      data: result === undefined || '' ? [] : result,
    },
    // {
    //   name: 'Penyelesaian',
    //   data: [20, 45, 95, 30, 44, 70, 80, 93, 25, 20, 255],
    // },
  ];
  return (
    <div>
      <div className="ml-5 mr-5 h-full">
        <Paper elevation={1} className="pr-5 w-full h-full">
          <Typography className="m-14 font-semibold" variant="subtitle2">
            Jumlah Anggota
          </Typography>
          <hr className="ml-5 mr-5 border-1 border-inherit" />
          <ReactApexChart
            options={grafik}
            series={series}
            type="area"
            // width={800}
            height={400}
          />
        </Paper>
      </div>
    </div>
  );
}
