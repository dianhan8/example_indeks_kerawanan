/* eslint-disable array-callback-return */
/* eslint-disable import/prefer-default-export */
import { Paper, Typography } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';

export default function FakePieChart() {
  // pieChart
  const [pie, setPie] = useState();
  const result = [];
  const resultOptions = [];
  useEffect(() => {
    axios
      .get(`http://103.140.90.122:3080/atensi/paichart`)
      .then((res) => {
        setPie(res?.data?.data);
      })
      .catch((err) => {
        console.log(err, 'errPie');
      });
  }, []);

  pie?.map((item) => {
    const indexs = resultOptions.findIndex((element) => element === item?.keterangan?.toString());
    if (indexs !== -1) {
      result[indexs] = result[indexs] ? result[indexs] + 1 : 1;
    } else {
      result[resultOptions?.length] = result[resultOptions?.length]
        ? result[resultOptions?.length] + 1
        : 1;
      if (item?.keterangan) {
        resultOptions.push(item.keterangan);
      } else if (!resultOptions.includes('item lainnya')) {
        resultOptions.push('item lainnya');
      }
      // resultOptions.push(item?.keterangan?.toString());
    }
    // end PieChart
  });
  console.log(result, 'result');
  console.log(resultOptions, 'resultOptions');

  // pie chart
  const options = {
    plotOptions: {
      pie: {
        expandOnClick: false,
        donut: {
          size: '55px',
          labels: {
            show: true,
            total: {
              show: true,
              showAlways: true,
              fontSize: '24px',
              color: '#2787AB',
            },
          },
        },
      },
    },
    legend: {
      position: 'right',
      width: 300,
    },
    states: {
      hover: {
        filter: {
          type: '',
        },
      },
    },
    theme: {
      // opaciti
      // monochrome: {
      //   enabled: true,
      //   shadeTo: "light",
      //   shadeIntensity: 0.65,
      // },
    },
    fill: {
      opacity: 1,
    },
    stroke: {
      width: 2,
      colors: undefined,
    },
    series: result,
    responsive: [
      {
        breakpoint: 1000,
        options: {
          chart: {
            width: 300,
          },
          plotOptions: {
            bar: {
              horizontal: false,
            },
          },
          legend: {
            show: false,
            position: 'bottom',
          },
        },
      },
    ],
    // {
    //   Today: [
    //     {
    //       data: [92.8, 6.1, 1.1],
    //       change: [-0.6, 0.7, 0.1],
    //     },
    //   ],
    //   Yesterday: [
    //     {
    //       data: [77.2, 8.4, 14.4],
    //       change: [-2.3, 0.3, -0.2],
    //     },
    //   ],
    //   "Last 7 days": [
    //     {
    //       data: [88.2, 9.2, 2.6],
    //       change: [1.9, -0.4, 0.3],
    //     },
    //   ],
    //   "Last 28 days": [
    //     {
    //       data: [65.2, 2.6, 32.2],
    //       change: [-12.6, -0.7, 4.2],
    //     },
    //   ],
    //   "Last 90 days": [
    //     {
    //       data: [93.5, 4.2, 2.3],
    //       change: [2.6, -0.7, 2.1],
    //     },
    //   ],
    // },
    labels: resultOptions === undefined || '' ? [] : resultOptions,
  };

  const seriess = result === undefined || '' ? [] : result;
  return (
    <div>
      <div className="m-5 ">
        <Paper elevation={1} className="pr-5 w-full h-full">
          <Typography className=" mb-10 mt-14 ml-14 font-semibold" variant="subtitle2">
            Jumlah Anggota
          </Typography>
          <hr className="ml-5 mr-5 border-1 border-inherit" />
          <ReactApexChart
            options={options}
            series={seriess}
            type="donut"
            // className="md:w-32 lg:w-full"
            width={800}
            // height={600}
          />
          {/* <div>
            {options.series.map((item) => (
              <div>{item}</div>
            ))}
          </div> */}
        </Paper>
      </div>
    </div>
  );
}
