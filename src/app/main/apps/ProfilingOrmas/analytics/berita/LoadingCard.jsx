import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';
import { MdChevronLeft, MdChevronRight } from 'react-icons/md';

export default function LoadingCards({ dataLoading }) {
  const slideLeft = () => {
    const slider = document.getElementById('slider');
    slider.scrollLeft -= 500;
  };
  const slideRight = () => {
    const slider = document.getElementById('slider');
    slider.scrollLeft += 500;
  };
  console.log(dataLoading, 'dataLoading');
  return (
    <div>
      <div className="relative flex items-center group">
        <MdChevronLeft
          onClick={slideLeft}
          className="bg-white left-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block"
          size={40}
        />
        <div
          id="slider"
          className="flex flex-nowrap w-full h-full overflow-x-scroll whitespace-nowrap scroll-smooth scrollbar-hide relative"
        >
          {Array(dataLoading)
            .fill(0)
            .map((_, items) => (
              <Stack spacing={1} className="m-10" key={items}>
                <Skeleton variant="rectangular" width={210} height={100} className="rounded-t-lg" />
                <Skeleton variant="rounded" width={210} height={100} />
                <Skeleton variant="circular" width={40} height={40} />
              </Stack>
            ))}
          ;
        </div>
        <MdChevronRight
          onClick={slideRight}
          className="bg-white right-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block"
          size={40}
        />
      </div>
    </div>
  );
}
