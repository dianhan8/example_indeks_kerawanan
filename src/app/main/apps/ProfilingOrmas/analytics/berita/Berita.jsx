/* eslint-disable no-undef */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import moment from 'moment';
import { useEffect, useState } from 'react';
import '../dashboard.css';
import { MdChevronLeft, MdChevronRight } from 'react-icons/md';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions } from '@mui/material';
import FuseAnimate from '@fuse/core/FuseAnimate';
import imgDefault from './imgDefault.jpg';
import LoadingCards from './LoadingCard';

const styles = (theme) => ({
  Card: {
    width: 300,
    margin: 'auto',
  },
  Media: {
    height: 100,
    width: '100%',
  },
});

export const Berita = () => {
  const [data, setData] = useState();
  const [content, setContent] = useState();
  const [title, setTitle] = useState();
  const [img, setImg] = useState();
  const [sumber, setSumber] = useState();
  const [tanggal, setTanggal] = useState(moment().format('DD-MM-YYYY'));
  const [dekripsi, setDekripsi] = useState();
  const [berita, setBerita] = useState();
  const [loading, setLoading] = useState();

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://newsapi.org/v2/top-headlines?country=id&apiKey=268c8041b9c54da18be9ed34dfc5d408`
      )
      .then((res) => {
        setLoading(false);
        // console.log(res, 'res');
        // eslint-disable-next-line array-callback-return
        setTitle(res?.data?.articles?.title);
        setImg(res?.data?.articles?.urlToImage);
        setBerita(res?.data?.articles);
        setData(res?.data?.articles);
        setContent(res?.data?.articles?.content);
        setTitle(res?.data?.articles?.title);
        // setSumber(res.data.articles.source.name)
        setDekripsi(res?.data?.articles?.description);
      })
      .catch((err) => {
        setLoading(false);
        // console.log(err, 'erorBerita');
      });
  }, []);
  // useEffect(() => {
  //   axios.get(fetchURL).then((response) => {
  //     setMovies(response.data.results);
  //   });
  // }, [fetchURL]);

  const slideLeft = () => {
    const slider = document.getElementById('slider');
    slider.scrollLeft -= 500;
  };
  const slideRight = () => {
    const slider = document.getElementById('slider');
    slider.scrollLeft += 500;
  };

  if (!data) {
    return <LoadingCards dataLoading={25} />;
  }

  return (
    <div>
      <FuseAnimate animation="transition.expandIn" delay={1300}>
        <div className="body">
          <div className="marquee">
            <div>Berita Indonesia Terkini Tanggal {tanggal}</div>
            <div>Berita Indonesia Terkini Tanggal {tanggal}</div>
          </div>
        </div>
      </FuseAnimate>
      <FuseAnimate animation="transition.expandIn" delay={1400}>
        <div>
          <div className="relative flex items-center group">
            <MdChevronLeft
              onClick={slideLeft}
              className="bg-white left-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block"
              size={40}
            />
            <div
              id="slider"
              className="flex flex-nowrap w-full h-full overflow-x-scroll whitespace-nowrap scroll-smooth scrollbar-hide relative"
            >
              {berita?.map((item) => {
                return (
                  <div className="flex flex-nowrap w-full ml-5 mr-5 mb-5">
                    <Card sx={{ maxWidth: 400 }}>
                      <CardActionArea>
                        <div
                          style={{ width: 400, height: 100 }}
                          className="flex items-center justify-center"
                        >
                          {item?.urlToImage === null ? (
                            <CardMedia
                              className="max-h-full max-w-full"
                              // style={styles.Media}
                              component="img"
                              // height="110"
                              image={imgDefault}
                              alt={item?.source?.name}
                            />
                          ) : (
                            <CardMedia
                              className="max-h-full max-w-full"
                              // style={styles.Media}
                              component="img"
                              // height="110"
                              image={item?.urlToImage}
                              alt={item?.source?.name}
                            />
                          )}
                        </div>
                        <CardContent>
                          <Typography gutterBottom variant="h6" component="div">
                            {item?.title !== null
                              ? item?.title.slice(0, 35).concat(['...'])
                              : 'Judul Kosong'}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            {item?.description !== null
                              ? item?.description.slice(0, 55).concat(['...'])
                              : 'Keterangan Kosong'}
                          </Typography>
                          <div className="flex justify-end">
                            <Typography variant="caption" color="text.secondary">
                              {item?.source?.name !== null
                                ? item.source?.name
                                : 'Waktu Tidak Di Ketahui'}
                            </Typography>
                          </div>
                        </CardContent>
                      </CardActionArea>
                      <CardActions>
                        <Button
                          variant="contained"
                          size="small"
                          // color="warning"
                          href={item?.url}
                          target="_blank"
                        >
                          Selengkapnya
                        </Button>
                      </CardActions>
                    </Card>
                  </div>
                );
              })}
            </div>
            <MdChevronRight
              onClick={slideRight}
              className="bg-white right-0 rounded-full absolute opacity-50 hover:opacity-100 cursor-pointer z-10 hidden group-hover:block"
              size={40}
            />
          </div>
        </div>
      </FuseAnimate>
      {/* <Test img={img} title={title} /> */}
      {/* {loading === true ?(
<div>loading</div>
                ): berita !== 0 ?(

                ) : (

                )} */}
    </div>
  );
};
