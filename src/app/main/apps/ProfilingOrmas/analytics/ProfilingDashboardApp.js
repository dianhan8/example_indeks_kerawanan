import withReducer from 'app/store/withReducer';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from '@lodash';
import { motion } from 'framer-motion';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { Card, Typography } from '@mui/material';
import reducer from './store';
import { selectWidgetsEntities, getWidgets } from './store/widgetsSlice';
import JumlahBencanaCard from './widgets/cardHeader/JumlahBencanaCard';
import JumlahPelanggaranCard from './widgets/cardHeader/JumlahPelanggaranCard';
import JumlahGangguanPolriCard from './widgets/cardHeader/JumlahGangguanPolriCard';
import JumlahSelesaiPolriCard from './widgets/cardHeader/JumlahSelesaiPolriCard';
import Widget5 from './widgets/Widget5';
import JumlahBelumSelesaiCard from './widgets/cardHeader/JumlahBelumSelesaiCard';
import JumlahKejahatanPolriCard from './widgets/cardHeader/JumlahKejahatanPolriCard';
import ProfilingDashboardAppHeader from './ProfilingDashboardAppHeader';
import PeriodesasiData from './componentAssets/PeriodesasiData';
import DialogFilter from './filter/DialogFilter';
import Covid19 from '../../PETA/components/Covid19';
import { Berita } from './berita/Berita';
import SosialMedia from './berita/dataAnalitycSoisalMedia/SosialMedia';
import { DataPrediksiPage } from './dataPrediksi/DataPrediksiPage';
import Widget6 from './widgets/Widget6';
import Widget7 from './widgets/Widget7';
import Widget8 from './widgets/Widget8';
import Widget1 from './widgets/Widget1';
import { Box } from '@material-ui/core';
// import Covid19 from "../../PETA/componen/Covid19";

function ProfilingDashboardApp() {
  const dispatch = useDispatch();
  const widgets = useSelector(selectWidgetsEntities);

  useEffect(() => {
    dispatch(getWidgets());
  }, [dispatch]);

  if (_.isEmpty(widgets)) {
    return null;
  }

  const container = {
    show: {
      transition: {
        staggerChildren: 0.06,
      },
    },
  };

  const item = {
    hidden: { opacity: 0, y: 20 },
    show: { opacity: 1, y: 0 },
  };

  return (
    <div className="w-full">
      <ProfilingDashboardAppHeader />
      <div>
        {/* <div>
          <Berita />
        </div> */}
        <div className="flex justify-start md:flex-wrap m-2">
          <div>
            <PeriodesasiData />
          </div>
          <div>
            <DialogFilter />
          </div>
          {/* <div>
            <DialogFilter />
          </div> */}
        </div>
        <motion.div
        className="flex flex-col md:flex-row sm:p-2 container"
        variants={container}
        initial="hidden"
        animate="show"
      >
      <motion.div variants={item} className="widget w-full p-16 pb-48">
        <Widget6 data={widgets.widget6} />
      </motion.div>
   
      <motion.div variants={item} className="widget w-full p-16 pb-48">
        <Widget5 data={widgets.widget5} />
      </motion.div>
      </motion.div>
      </div>

      <motion.div
        className="flex flex-col md:flex-row sm:p-2 container"
        variants={container}
        initial="hidden"
        animate="show"
      >
      <motion.div variants={item} className="widget w-full p-16 pb-50">
        <Widget7 data={widgets.widget7} />
      </motion.div>
   
      <motion.div variants={item} className="widget w-full p-16 pb-48">
        <Widget8 data={widgets.widget8} />
      </motion.div>
      </motion.div>

      <motion.div
        className="flex flex-col md:flex-row sm:p-2 container"
        variants={container}
        initial="hidden"
        animate="show"
      >

      <motion.div variants={item} className="widget w-full p-16 pb-50">
        <Widget7 data={widgets.widget7} />
      </motion.div>
   
      <motion.div variants={item} className="widget w-full p-16 pb-48">
        <Widget8 data={widgets.widget8} />
      </motion.div>
      </motion.div>

      <Box>
      <motion.div variants={item} className="widget w-full p-10 pb-16">
      <Typography variant="h3" className="text-20 sm:text-24 md:text-32 font-bold leading-none mb-8 tracking-tight">
        Peta Persebaran Kegiatan Meresahkan
      </Typography>
        <Covid19 />
      </motion.div></Box>

      <motion.div variants={item} className="widget w-full p-10 pb-16">
        <Card>
      <Widget1 data={widgets.widget1} />
      </Card>
      </motion.div>
    </div>
  );
}

export default withReducer('ProfilingDashboardApp', reducer)(ProfilingDashboardApp);
