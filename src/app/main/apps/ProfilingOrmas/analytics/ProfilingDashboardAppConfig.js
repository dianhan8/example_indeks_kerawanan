import { lazy } from 'react';

const ProfilingDashboardApp = lazy(() => import('./ProfilingDashboardApp'));

const ProfilingDashboardAppConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'apps/ProfilingOrmas/analytics',
      element: <ProfilingDashboardApp />,
    },
  ],
};

export default ProfilingDashboardAppConfig;
