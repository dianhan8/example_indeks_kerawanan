import _ from '@lodash';
import Card from '@mui/material/Card';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
// import ReactApexChart from 'react-apexcharts';
import Box from '@mui/material/Box';
import Chart from 'react-apexcharts';


function Widget5() 

  {
    const [state, setState] = useState({
        options: {
            dataLabels : {
                enabled: false
            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            xaxis      : {
                categories: ['Ideology', 'Polotic', 'Sosial Culture', 'Law Enforcement', 'Devense', 'Scurity']
            }
        },
        series : [
            {
                data: [30, 40, 25, 50, 49, 21]
            }
        ],
    });

    

  return (
    <Card className="w-full rounded-20 shadow">
        <Card className="w-full rounded-20 shadow">
        <div className="bar">
        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
          Level Indeks Kerawanan
            </Typography>
        </div>
            <Chart options={state.options} series={state.series} type="bar" width="435"/>
        </div>
        </Card>      
    </Card>
  );
}

export default memo(Widget5);

