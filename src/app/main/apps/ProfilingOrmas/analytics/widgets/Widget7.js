import _ from '@lodash';
import Card from '@mui/material/Card';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
// import ReactApexChart from 'react-apexcharts';
import Box from '@mui/material/Box';
import Chart from 'react-apexcharts';


function Widget7() 

  {
    const [state, setState] = useState({
        options: {
            dataLabels : {
                enabled: false
            },
            plotOptions: {
                bar: {
                    horizontal: true
                }
            },
            xaxis      : {
                categories: ['1. DKI Jakarta', '2. Jawa Barat', '3. Jawa Tengah', '4. Jawa Timur', '5. Banten', '6. Aceh', '7. Pontianak', '8. Banjarmasin', '9. Makasar', '10. Banten' ]
            }
        },
        series : [
            {
                data: [110, 95, 80, 70, 60, 50, 40, 30, 20, 10]
            }
        ],
    });

    

  return (
    <Card className="w-full rounded-20 shadow">
        <Card className="w-full rounded-20 shadow">
        <div className="bar">
        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
            Trend Level Indeks Kerawanan
            </Typography>
        </div>
            <Chart options={state.options} series={state.series} type="bar" width="435"/>
        </div>
        </Card>      
    </Card>
  );
}

export default memo(Widget7);

