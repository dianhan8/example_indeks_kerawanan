import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import { Card } from '@material-ui/core';
import Typography from '@mui/material/Typography';


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function createData(name, calories, fat, carbs, protein, provinsi, link) {
  return { name, calories, fat, carbs, protein, provinsi, link };
}

const rows = [
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Facebook', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22','DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22','DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22','DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
  createData('Ormas 1', 'Negativ', 'Liputan 6', 'Penyerangan Posko FB..', '05/05/22', 'DKI Jakarta', 'https://liputan6.id..'),
];

export default function Widget1() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Card className="w-full rounded-20 shadow">
       <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
            Isu Terkini
            </Typography>
        </div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Nama Ormas</StyledTableCell>
            <StyledTableCell align="right">Sentimen</StyledTableCell>
            <StyledTableCell align="right">Media</StyledTableCell>
            <StyledTableCell align="right">Judul Artikel</StyledTableCell>
            <StyledTableCell align="right">Tanggal</StyledTableCell>
            <StyledTableCell align="right">Provinsi</StyledTableCell>
            <StyledTableCell align="right">Link</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <StyledTableCell align="right">{row.calories}</StyledTableCell>
              <StyledTableCell align="right">{row.fat}</StyledTableCell>
              <StyledTableCell align="right">{row.carbs}</StyledTableCell>
              <StyledTableCell align="right">{row.protein}</StyledTableCell>
              <StyledTableCell align="right">{row.provinsi}</StyledTableCell>
              <StyledTableCell align="right">{row.link}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={3}
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: {
                  'aria-label': 'rows per page',
                },
                native: true,
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
    </Card>
  );
}