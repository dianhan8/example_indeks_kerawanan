import _ from '@lodash';
import Card from '@mui/material/Card';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { memo, useState } from 'react';
// import ReactApexChart from 'react-apexcharts';
import Box from '@mui/material/Box';
import Chart from 'react-apexcharts';
import { Stack } from '@mui/material';


function Widget6() 

{
  const [state, setState] = useState({
      options: {
          labels: ['Positive', 'Neutral', 'Negative' ]
      },
      series : [48.8, 12.3, 39.9]

  });
    

  return (
    <Card className="w-full rounded-25 shadow">
       
        <div className="bar">
        <div className="flex w-full flex-col">
          <Typography className="h2 sm:h2 font-hard text-bold relative p-20 flex flex-row items-center justify-between">
           Profil Ormas
            </Typography>
        </div>
        
        {/* <Chart options={state.options} series={state.series} type="donut" width="425"/> */}
        </div>
        <Card >
        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row justify-between" variant="body1" fontWeight="700">
           Nama <Stack className="flex flex-row justify-center">: Ormas A </Stack>
            </Typography>
        </div>

        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
           Wilayah Operasi  <td><Stack className="">: DKI Jajarta </Stack></td>
            </Typography>
        </div>

        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
           Tahun Berdiri <Stack className="">: 2022 </Stack>
            </Typography>
        </div>

        <div className="flex flex-col">
          <Typography className="h3 sm:h2 font-medium relative p-20 flex flex-row items-center justify-between">
           Kategori Kegiatan <Stack className="">: Politik </Stack>
            </Typography>
        </div>
        </Card>
        </Card>      
   
  );
}

export default memo(Widget6);

