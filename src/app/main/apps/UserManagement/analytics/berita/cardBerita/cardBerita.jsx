/* eslint-disable import/prefer-default-export */

export const CardBerita = (props) => {
  console.log(props, 'Berita');
  return (
    <div>
      <div className="w-[160px] sm:w-[200px] md:w-[240px] lg:w-[280px] inline-block cursor-pointer relative p-">
        <img
          className="w-full h-auto block"
          src={props.item?.urlToImage}
          alt={props?.item?.title}
        />
        <div className="absolute top-0 left-0 w-full h-full hover:bg-black/80 opacity-0 hover:opacity-100 text-white">
          <p className="white-space-normal text-5 md:text-sm flex justify-center items-center h-full text-center">
            <div>
              <h1>{props?.item?.title}</h1>
            </div>
          </p>
          <h1>Rating Anda </h1>
          <h1>Telah di tonton Orang</h1>
        </div>
      </div>
    </div>
  );
};
