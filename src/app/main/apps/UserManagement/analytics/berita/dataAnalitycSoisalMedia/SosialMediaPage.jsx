import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import axios from 'axios';
import { Link } from '@mui/material';

const arrFacebook = [
  { nama: '#KasusSambo' },
  { nama: '#MinyakNaik' },
  { nama: '#KelapaSwit' },
  { nama: '#OnePieceRED' },
  { nama: '#PerangRusia' },
  { nama: '#HargaMinyakMentah' },
  { nama: '#Bansos' },
  { nama: '#PerjuanganRakyat' },
  { nama: '#UsustTuntas' },
  { nama: '#NegaraYangLucu' },
];
const arrTwitter = [
  { nama: '#turunkanBBM' },
  { nama: '#KasusSambo' },
  { nama: '#PerangRusia' },
  { nama: '#OnePieceRED' },
  { nama: '#NegaraYangLucu' },
  { nama: '#Bansos' },
  { nama: '#KelapaSwit' },
  { nama: '#PerjuanganRakyat' },
  { nama: '#HargaMinyakMentah' },
  { nama: '#UsustTuntas' },
];
const arrInstagram = [
  { nama: '#PerjuanganRakyat' },
  { nama: '#OnePieceRED' },
  { nama: '#KelapaSwit' },
  { nama: '#PerangRusia' },
  { nama: '#HargaMinyakMentah' },
  { nama: '#NegaraYangLucu' },
  { nama: '#Bansos' },
  { nama: '#MinyakNaik' },
  { nama: '#UsustTuntas' },
  { nama: '#KasusSambo' },
];
const arrYoutube = [
  { nama: '#TolakBBM' },
  { nama: '#NegaraYangLucu' },
  { nama: '#PerjuanganRakyat' },
  { nama: '#OnePieceRED' },
  { nama: '#PerangRusia' },
  { nama: '#HargaMinyakMentah' },
  { nama: '#KelapaSwit' },
  { nama: '#Bansos' },
  { nama: '#KasusSambo' },
  { nama: '#UsustTuntas' },
];

export default function SosialMediaPage() {
  const [expanded, setExpanded] = React.useState(false);
  const [dataYoutube, setDataYoutube] = React.useState();
  console.log(dataYoutube, "TREN YT");

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  React.useEffect(() => {
    axios.get('https://youtube.googleapis.com/youtube/v3/videos?part=snippet&chart=mostPopular&maxResults=10&regionCode=ID&key=AIzaSyAea-5QrCQ2ZN49AjJS9XcAel4Zz-d9ogs').then((res) => {
      setDataYoutube(res?.data?.items);
    }).catch((err) => {
      console.log(err, 'ERR')
    })
  }, [])

  return (
    <div>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel0')}>
        <AccordionSummary>
          <Typography sx={{ color: 'text.secondary' }}>Trending Top-10</Typography>
        </AccordionSummary>
      </Accordion>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}>
            <FacebookIcon />
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>#KasusSambo</Typography>
        </AccordionSummary>
        {arrFacebook?.map((item, index) => (
          <AccordionDetails>
            <Typography>
              {index + 1}. {item?.nama}
            </Typography>
          </AccordionDetails>
        ))}
      </Accordion>
      <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}>
            <InstagramIcon />
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>#turunkanBBM</Typography>
        </AccordionSummary>
        {arrTwitter?.map((item, index) => (
          <AccordionDetails>
            <Typography>
              {index + 1}. {item?.nama}
            </Typography>
          </AccordionDetails>
        ))}
      </Accordion>
      <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}>
            <TwitterIcon />
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>#perjuanganrakyat</Typography>
        </AccordionSummary>
        {arrInstagram?.map((item, index) => (
          <AccordionDetails>
            <Typography>
              {index + 1}. {item?.nama}
            </Typography>
          </AccordionDetails>
        ))}
      </Accordion>
      <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography sx={{ width: '33%', flexShrink: 0 }}>
            <YouTubeIcon />
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>#trendingYotube</Typography>
        </AccordionSummary>
        {dataYoutube?.map((item, index) => (
          <AccordionDetails>
            <Typography>
             {index + 1}.  #{item?.snippet?.title.slice(0, 50).concat(['...'])}

              {/* <Link href={item?.snippet?.thumbnails?.high
                ?.url} underline="hover">
                Lihat Selanjunya
              </Link> */}
            </Typography>
          </AccordionDetails>
        ))}
      </Accordion>
    </div>
  );
}
