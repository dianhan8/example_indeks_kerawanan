/* eslint-disable no-unused-expressions */
/* eslint-disable array-callback-return */
/* eslint-disable no-constant-condition */
import { Paper } from '@mui/material';
import { useState } from 'react';
import Chart from 'react-apexcharts';

// eslint-disable-next-line import/prefer-default-export
export const ChartGlobalPrediktif = (props) => {
  // console.log(props, 'props');

  const allKejahatan = [];
  const allPenyelesaian = [];
  allKejahatan.push(
    props?.resultdataJumlahKejahatan21,
    props?.resultdataJumlahKejahatan22,
    props?.resultdataJumlahKejahatan23
  );
  allPenyelesaian.push(
    props?.resultdataJumlahPenyelesaian21,
    props?.resultdataJumlahPenyelesaian22,
    props?.resultdataJumlahPenyelesaian23
  );

  const mapAllPenyelesaian = allPenyelesaian.map((i) => {
    if (i === undefined) {
      ('maulana');
      console.log('satu');
    } else {
      ('muhamad');
      console.log('dua');
    }
  });
  // console.log(mapAllPenyelesaian);
  // console.log(allKejahatan, 'kejahatan');
  // console.log(allPenyelesaian, 'penyelesaian');
  const [option, setOption] = useState({
    chart: {
      type: 'bar',
      height: '100%',
      stacked: true,
      foreColor: '#999',
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    stroke: {
      curve: 'smooth',
      width: 3,
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
      strokeColor: '#fff',
      strokeWidth: 3,
      strokeOpacity: 1,
      fillOpacity: 1,
      hover: {
        size: 6,
      },
    },
    xaxis: {
      categories: [
        '2022',
        '2023',
        '2024',
        // 'April',
        // 'Mei',
        // 'Juni',
        // 'Juli',
        // 'Agustus',
        // 'September',
        // 'Oktober',
        // 'November',
        // 'Desember',
      ],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      tooltip: {
        enabled: true,
      },
    },
    grid: {
      position: 'back',
    },
    legend: {
      show: false,
    },
    fill: {
      type: 'solid',
      opacity: 0.7,
    },
    tooltip: {
      followCursor: true,
      theme: 'dark',
      fixed: {
        enabled: false,
        position: 'topRight',
        offsetX: 0,
        offsetY: 0,
      },
    },
  });
  // series
  const [seriess, setSerieses] = useState([
    {
      name: 'Ganjar Pranowo',
      data:
        [
          props?.resultdataJumlahPenyelesaian21,
          props?.resultdataJumlahPenyelesaian22,
          props?.resultdataJumlahPenyelesaian23,
        ] === ''
          ? []
          : [
              props?.resultdataJumlahPenyelesaian21,
              props?.resultdataJumlahPenyelesaian22,
              props?.resultdataJumlahPenyelesaian23,
            ],
    },
    {
      name:
        allPenyelesaian.length === 0
          ? allPenyelesaian.map((i) => {
              if (i === undefined) {
                ('Prediksi');
                // console.log('first');
              } else {
                ('Jumlah Kejahatan');
                // console.log('second');
              }
            })
          : 'Anies Baswedan',
      data:
        [
          props?.resultdataJumlahKejahatan21,
          props?.resultdataJumlahKejahatan22,
          props?.resultdataJumlahKejahatan23,
        ] === ''
          ? []
          : [
              props?.resultdataJumlahKejahatan21,
              props?.resultdataJumlahKejahatan22,
              props?.resultdataJumlahKejahatan23,
            ],
    },
    // {
    //   name: 'Jumlah Penyelesaian',
    //   data: allPenyelesaian === '' ? [] : allPenyelesaian,
    // },
    // {
    //   name: 'Jumlah Kejahatan',
    //   data: allKejahatan === '' ? [] : allKejahatan,
    // },
    // {
    //   name: '2021 Jumlah Data kejahatan',
    //   data: [30, 35, 75, 5, 42, 75, 60, 70, 28, 67, 234, 504],
    // },
    // {
    //   name: '2022 Jumlah Data kejahatan',
    //   data: [20, 45, 95, 30, 44, 70, 80, 93, 25, 20, 255, 389],
    // },
  ]);

  const [loading, setLoading] = useState(false);

  return (
    <div className="ml-10 mt-10">
      {/* {loading ? (
        <div className="flex justify-center items-center h-full w-full">
          <CircularProgress />
        </div>
      ) : ( */}
      <Paper className="mr-10">
        {/* <div className="flex flex-col">
            <Typography className="h3 sm:h2 font-medium ml-5 mt-5">
              Grafik Data Kejahatan
            </Typography>
          </div> */}
        <Chart options={option} series={seriess} type="bar" height={400} />;
      </Paper>
      {/* )} */}
    </div>
  );
};
