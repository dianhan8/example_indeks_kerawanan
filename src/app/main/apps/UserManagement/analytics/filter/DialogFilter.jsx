import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import {Typography} from "@mui/material";
import { FilterDataDasboard } from './FilterDataDasboard';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function DialogFilter() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = () => {
    setOpen(false);
  };

  return (
    <div className="flex w-full container">
    <div className="flex flex-col sm:flex-row flex-auto sm:items-center min-w-0 p-6 md:p-8 pb-0 md:pb-0">
          <div className="flex items-center mt-24 sm:mt-0 sm:mx-8 space-x-12">
          <Button
          className="whitespace-nowrap"
          variant="contained"
          color="secondary"
          // path="apps/UserManagement/analytics/widgest/TambahUser"
          // startIcon={<FuseSvgIcon size={20}>heroicons-solid:save</FuseSvgIcon>}
        >
          Tambah User
        </Button>
        </div>
        </div>
      </div>
  );
}
