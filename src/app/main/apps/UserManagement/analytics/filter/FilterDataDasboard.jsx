import { DesktopDatePicker, LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import {
  AppBar,
  Autocomplete,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Paper,
  Stack,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { ChartGlobalPrediktif } from "../dataPrediksi/data/ChartGlobalPrediktif";
import AppGrafik from "./AppGrafik";
import ChartDummy from "./chart/ChartDummy";
import TotalChartDummy from "./chart/TotalChartDummy";
// import AnalyticsDashboardApp from "../AnalyticsDashboardApp";
// import AppGrafik from "./AppGrafik";

const options1 = ["Anies Baswedan", "Ganjar Pranowo", "Prabowo Subianto", "Erik Thohir", "Puan Maharani"];
const options2 = [""];

export const FilterDataDasboard = () => {
  const [mulai, setMulai] = useState(moment().format("YYYY-MM-DD"));
  const [akhir, setAkhir] = useState(moment().format("YYYY-MM-DD"));
  ;
  const [kolomWaktu, setKolomWaktu] = useState("");
  const [namaSubGolongan, SetNamaSubGolongan] = useState("");
  const [statusLaporan, setStatusLaporan] = useState("");
  const [polda, setPolda] = useState("");
  const [polres, setPolres] = useState("");
  const [polsek, setPolsek] = useState("");
  const [value, setValue] = useState(options1[0]);
  const [inputValue, setInputValue] = useState(options2[0]);

  const handleChangeTanggalAwal = (newValue) => {
    setMulai(newValue);
  };
  const handleChangeTanggalAkhir = (newValue) => {
    setAkhir(newValue);
  };
  const handleKolomWaktu = (koloWaktuValue) => {
    setKolomWaktu(koloWaktuValue);
  };

  const handleSubmit = () => {
    
    setMulai("");
    setAkhir("");
    setKolomWaktu("");
    SetNamaSubGolongan("");
    setStatusLaporan("");
    setPolda("");
    setPolres("");
    setPolsek("");
  };

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Paper elevation={2} className=""> 
    <div className="p-10">
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <div className="flex justify-center gap-10">
            <Stack direction="row" spacing={2}>
              <Button>7 Hari Terahkir</Button>
            </Stack>
            </div>
          </LocalizationProvider>
        </div>
        <div className="p-10">
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <div className="flex justify-center gap-10">
            <Stack direction="row" spacing={2}>
              <Button>30 Hari Terahkir</Button>
            </Stack>
            </div>
          </LocalizationProvider>
        </div>
        <div className="p-10">
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <div className="flex justify-center gap-10">
            <Stack direction="row" spacing={2}>
              <Button>1 Tahun Terahkir</Button>
            </Stack>
            </div>
          </LocalizationProvider>
        </div>

        <Typography> Dari </Typography>
        <div className="p-10">
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <div className="flex justify-center gap-10">
              <DesktopDatePicker
                size="small"
                label="Dari"
                // inputFormat="MM/dd/yyyy"
                value={mulai}
                onChange={handleChangeTanggalAwal}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </LocalizationProvider>
        </div>

        <Typography> Sampai </Typography>
        <div className="p-10">
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <div className="flex justify-center gap-10">
              <DesktopDatePicker
                label="Sampai"
                // inputFormat="MM/dd/yyyy"
                value={akhir}
                onChange={handleChangeTanggalAkhir}
                renderInput={(params) => <TextField {...params} />}
              />
            </div>
          </LocalizationProvider>
        </div>
        {/* <div>
          <div >
            <Dialog
              container spacing={2}
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="xl"
              fullWidth
            >
              <DialogActions>
                <Button variant="contained" onClick={handleClose} autoFocus>
                  Tutup
                </Button>
              </DialogActions>
            </Dialog>
          </div>
          <div >
            <Dialog
              container spacing={2}
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="xl"
              fullWidth
            >
              <DialogActions>
                <Button variant="contained" onClick={handleClose} autoFocus>
                  Simpan
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        </div> */}
    </Paper>
  );
};
