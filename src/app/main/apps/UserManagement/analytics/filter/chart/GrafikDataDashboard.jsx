import { CircularProgress, Paper, Typography } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

export const GrafikDataDashboard = () => {
  const [option, setOption] = useState({
    chart: {
      type: "area",
      height: "100%",
      stacked: true,
      foreColor: "#999",
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    stroke: {
      curve: "smooth",
      width: 3,
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
      strokeColor: "#fff",
      strokeWidth: 3,
      strokeOpacity: 1,
      fillOpacity: 1,
      hover: {
        size: 6,
      },
    },
    xaxis: {
      categories: [
        "Januari",
        "Febuari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember",
      ],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      tooltip: {
        enabled: true,
      },
    },
    grid: {
      position: "back",
    },
    legend: {
      show: false,
    },
    fill: {
      type: "solid",
      opacity: 0.7,
    },
    tooltip: {
      followCursor: true,
      theme: "dark",
      fixed: {
        enabled: false,
        position: "topRight",
        offsetX: 0,
        offsetY: 0,
      },
    },
  });
  // series
  const [seriess, setSerieses] = useState([
    {
      name: "2020 Jumlah Data Intelijen",
      data: [30, 70, 365, 50, 49, 60, 200, 391, 125, 202, 205, 793],
    },
    {
      name: "2021 Jumlah Data Intelijen",
      data: [90, 35, 475, 5, 42, 75, 130, 470, 28, 67, 234, 1010],
    },
    {
      name: "2022 Jumlah Data Intelijen",
      data: [56, 45, 395, 30, 44, 70, 80, 93, 225, 420, 255, 389],
    },
  ]);

  const [loading, setLoading] = useState(false);

  return (
    <div className="ml-10 mt-10">
      {loading ? (
        <div className="flex justify-center items-center h-full w-full">
          <CircularProgress />
        </div>
      ) : (
        <Paper className="mr-10">
          <div className="flex flex-col">
            <Typography className="h3 sm:h2 font-medium ml-5 mt-5">
              Grafik Data Intelijen
            </Typography>
          </div>
          <Chart options={option} series={seriess} type="area" height={400} />;
        </Paper>
      )}
    </div>
  );
};
