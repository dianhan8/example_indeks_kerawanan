import { Paper, Typography } from "@mui/material";
import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";

export const GrafikPieDikmaLantas = () => {


  // pie chart
  const options = {
    plotOptions: {
      pie: {
        expandOnClick: false,
        donut: {
          size: "55px",
          labels: {
            show: true,
            total: {
              show: true,
              showAlways: true,
              fontSize: "24px",
              color: "#2787AB",
            },
          },
        },
      },
    },
    states: {
      hover: {
        filter: {
          type: "",
        },
      },
    },
    theme: {
      // opaciti
      // monochrome: {
      //   enabled: true,
      //   shadeTo: "light",
      //   shadeIntensity: 0.65,
      // },
    },
    fill: {
      opacity: 1,
    },
    stroke: {
      width: 2,
      colors: undefined,
    },
    series: [77.2, 8.4, 14.4],
    // {
    //   Today: [
    //     {
    //       data: [92.8, 6.1, 1.1],
    //       change: [-0.6, 0.7, 0.1],
    //     },
    //   ],
    //   Yesterday: [
    //     {
    //       data: [77.2, 8.4, 14.4],
    //       change: [-2.3, 0.3, -0.2],
    //     },
    //   ],
    //   "Last 7 days": [
    //     {
    //       data: [88.2, 9.2, 2.6],
    //       change: [1.9, -0.4, 0.3],
    //     },
    //   ],
    //   "Last 28 days": [
    //     {
    //       data: [65.2, 2.6, 32.2],
    //       change: [-12.6, -0.7, 4.2],
    //     },
    //   ],
    //   "Last 90 days": [
    //     {
    //       data: [93.5, 4.2, 2.3],
    //       change: [2.6, -0.7, 2.1],
    //     },
    //   ],
    // },
    labels: [
      "Curanmor",
      "Penggelapan",
      "Narkotika",
      "Kecelakaan Lalin",
      "Pencurian",
      "pembunuhan",
    ],
  };

  const seriess = [77, 8, 14, 35, 67, 90];
  return (
    <div>
      <div className="m-5 h-auto">
        <Paper elevation={1} className="">
          <Typography className="mt-14 ml-14 font-semibold" variant="subtitle2">
            Jumlah Kejadian Menonjol
          </Typography>
          <hr className="ml-5 mr-5 border-1 border-inherit"></hr>
          <ReactApexChart
            options={options}
            series={seriess}
            type="donut"
            className="md:w-32 lg:w-full"
            // width={800}
            // height={600}
          />
          {/* <div>
            {options.series.map((item) => (
              <div>{item}</div>
            ))}
          </div> */}
        </Paper>
      </div>
    </div>
  );
};
