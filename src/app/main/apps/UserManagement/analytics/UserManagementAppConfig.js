import { lazy } from 'react';

const UserManagementApp = lazy(() => import('./UserManagementApp'));

const UserManagementAppConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'apps/UserManagement/analytics',
      element: <UserManagementApp />,
    },
  ],
};

export default UserManagementAppConfig;
