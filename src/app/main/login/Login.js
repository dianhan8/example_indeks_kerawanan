import Card from '@mui/material/Card';
import { styled, darken } from '@mui/material/styles';
import CardContent from '@mui/material/CardContent';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import { motion } from 'framer-motion';
import { useCallback, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import MabesLoginTab from './tabs/MabesLoginTab';
import PoldametrojayaLoginTab from './tabs/PoldametrojayaLoginTab';
import PolresLoginTab from './tabs/PolresLoginTab';
import PolsekLoginTab from './tabs/PolsekLoginTab';

const Root = styled('div')(({ theme }) => ({
  background: `linear-gradient(to right, ${theme.palette.primary.yellow} 0%, ${darken(
    theme.palette.primary.dark,
    0.5
  )} 100%)`,
  color: theme.palette.primary.contrastText,

  '& .Login-leftSection': {},

  '& .Login-rightSection': {
    background: `linear-gradient(to right, ${theme.palette.primary.light} 0%, ${darken(
      theme.palette.primary.dark,
      0.5
    )} 100%)`,
    color: theme.palette.primary.contrastText,
  },
}));

function Login() {
  const navigate = useNavigate();
  const [selectedTab, setSelectedTab] = useState(0);
  const API_URL = process.env.REACT_APP_BASE_URL;
  const [loginFailed, setLoginFailed] = useState('');
  const [errorMesaageUsername, setErrorMesaageUsername] = useState('');
  const [errorMessagePassword, setErrorMessagePassword] = useState('');
  const [login, setLogin] = useState({
    username: '',
    password: '',
    tenant: '',
  });
  const [loginMabes, setloginMabes] = useState([]);
  const [valueLoginMabes, setValueLoginMabes] = useState({
    id: '',
    name: '',
  });
  const [loginPolres, setloginPolres] = useState([]);
  const [valueLoginPolres, setValueLoginPolres] = useState({
    id: '',
    name: '',
  });
  const [loginPolda, setLoginPolda] = useState([]);
  const [valueLoginPolda, setValueLoginPolda] = useState({
    id: '',
    name: '',
  });
  const [loginPolsek, setLoginPolsek] = useState([]);
  const [valueLoginPolsek, setValueLoginPolsek] = useState({
    id: '',
    name: '',
  });

  const setValueMabes = useCallback((newValue) => {
    setValueLoginMabes(newValue);
  }, []);
  const setValuePolda = useCallback((newValue) => {
    setValueLoginPolda(newValue);
  }, []);
  const setValuePolres = useCallback((newValue) => {
    setValueLoginPolres(newValue);
  }, []);
  const setValuePolsek = useCallback((newValue) => {
    setValueLoginPolsek(newValue);
  }, []);
  const setState = useCallback((newValue) => {
    setLoginPolda(newValue);
  }, []);
  function handleTabChange(event, value) {
    setSelectedTab(value);
  }

  // Mabes Login
  useEffect(() => {
    axios
      .post('http://medialisa.id:3080/tenant-children', { parent: 'MP' })
      .then((res) => {
        // console.log(res.data, "res datatenant");
        setloginMabes(res.data);
        setValueLoginMabes({
          id: '',
          name: '',
        });
      })
      .catch((err) => {
        console.log(err);
        console.log('Data gak ketemu');
      });
  }, []);

  const callApi = ({ parent, role }) => {
    axios
      .post('http://medialisa.id:3080/tenant-children', { parent })
      .then((res) => {
        switch (role) {
          // case "mabes":
          //   setloginMabes((curr)=>res.data);

          //   break;
          case 'polda':
            setLoginPolda((curr) => res?.data);

            break;
          case 'polres':
            setloginPolres((curr) => res?.data);

            break;
          case 'polsek':
            setLoginPolsek((curr) => res?.data);
            break;

          default:
            break;
        }
      })
      .catch((err) => {
        switch (role) {
          case 'mabes':
            setloginMabes([]);

            break;
          case 'polda':
            setLoginPolda([]);

            break;
          case 'polres':
            setloginPolres([]);

            break;
          case 'polsek':
            setLoginPolsek([]);

            break;

          default:
            break;
        }
        console.log(err);
        console.log('Data gak ketemu');
      });
  };
  function getFilter(role) {
    let parent;
    switch (role) {
      case 'mabes':
        parent = 'MP';

        break;
      case 'polda':
        parent = valueLoginMabes?.idDors;

        break;
      case 'polres':
        parent = valueLoginPolda?.idDors;

        break;
      case 'polsek':
        parent = valueLoginPolres?.idDors;

        break;

      default:
        break;
    }
    callApi({ parent: parent?.toString(), role });
  }

  return (
    <Root className="flex flex-col flex-auto items-center justify-center shrink-0 p-16 md:p-24">
      <motion.div
        initial={{ opacity: 0, scale: 0.6 }}
        animate={{ opacity: 1, scale: 1 }}
        className="flex w-full max-w-400 md:max-w-3xl rounded-40 shadow-2xl overflow-hidden"
      >
        <Card
          className="Login-leftSection flex flex-col w-full max-w-sm items-center justify-center shadow-0"
          square
        >
          <CardContent className="flex flex-col items-center justify-center w-full max-w-320">
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1, transition: { delay: 0.2 } }}
            >
              {/* <div className="flex items-center mb-12">
                <img
                  className="logo-icon w-80"
                  src="assets/images/avatars/LOGOSC.png"
                  alt="logo"
                /> */}
                <div className="border-l-0 mr-6 w-20 h-20" />
                <div>
                  <Typography variant="h4" className="text-80 font-semibold logo-text text-center"  color="inherit">
                  LOGIN 
                  </Typography>
                  <Typography variant="h6" className="text-80 font-semibold logo-text text-center"  color="inherit">
                  INDEKS KERAWANAN
                  </Typography>
                  {/* <Typography
                    className="text-16 tracking-widest -mt-8 font-700"
                    color="textSecondary"
                  >
                    BPJS TK
                  </Typography> */}
                </div>
              {/* </div> */}
            </motion.div>

            <Tabs
              value={selectedTab}
              onChange={handleTabChange}
              variant="fullWidth"
              className="w-full mb-32"
            >
              {/* <Tab
                icon={
                  <img
                    className="h-40 p-4 bg-black rounded-12"
                    src="assets/images/logos/jwt.svg"
                    alt="firebase"
                  />
                }
                className="min-w-0"
                label="JWT"
              /> */}
              <Tab
                icon={
                  <img
                    className="h-80"
                    src="assets/images/avatars/garuda13.png"
                    alt="Data_Analytics"
                  />
                }
                className="min-w-0"
                label="Silahkan Masukan Username dan Password Anda"
                onClick={() => getFilter('bpjs')}
                // disabled={loginMabes.length === 0}
              />
              {/* <Tab
                // disabled
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="Polda"
                  />
                }
                className="min-w-0"
                label="Polda"
                onClick={() => getFilter('polda')}
                disabled={valueLoginMabes?.id === ''}
              />
              <Tab
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="Polres"
                  />
                }
                className="min-w-0"
                label="Polres"
                onClick={() => getFilter('polres')}
                disabled={valueLoginPolda?.id === ''}
              />
              <Tab
                // disabled
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="Polsek"
                  />
                }
                className="min-w-0"
                label="Polsek"
                onClick={() => getFilter('polsek')}
                disabled={valueLoginPolres?.id === ''}
              /> */}
              {/* <Tab
                icon={<img className="h-40" src="assets/images/logos/auth0.svg" alt="auth0" />}
                className="min-w-0"
                label="Auth0"
              /> */}
            </Tabs>

            {selectedTab === 0 && (
              <MabesLoginTab
                loginMabes={loginMabes}
                valueLoginMabes={valueLoginMabes}
                setValueMabes={setValueMabes}
              />
            )}
            {selectedTab === 1 && (
              <PoldametrojayaLoginTab
                loginPolda={loginPolda}
                setLoginPolda={setState}
                valueLoginPolda={valueLoginPolda}
                setValuePolda={setValuePolda}
              />
            )}
            {selectedTab === 2 && (
              <PolresLoginTab
                loginPolres={loginPolres}
                valueLoginPolres={valueLoginPolres}
                setValuePolres={setValuePolres}
              />
            )}
            {selectedTab === 3 && (
              <PolsekLoginTab
                loginPolsek={loginPolsek}
                valueLoginPolsek={valueLoginPolsek}
                setValuePolsek={setValuePolsek}
              />
            )}
            {/* {selectedTab === 0 && <JWTLoginTab />}
            {selectedTab === 1 && <JWTLoginTab />}
            {selectedTab === 2 && <JWTLoginTab />}
            {selectedTab === 3 && <JWTLoginTab />} */}
          </CardContent>

          <div className="flex flex-col items-center justify-center pb-10">
            <div>
              <span className="font-normal mr-8">Lupa Password?</span>
              <Link className="font-normal" to="/register">
                Buat Password Baru
              </Link>
            </div>
            <Link className="font-normal mt-8" to="/">
              Kembali Ke Dashboard
            </Link>
          </div>
        </Card>

        <div className="Login-rightSection hidden md:flex flex-1 items-center justify-center p-84">
          <div className="max-w-900 text-center">
            <div className="flex justify-center">
              <img
                className="flex items-center logo-icon w-224"
                src="assets/images/avatars/garuda13.png"
                alt="logo"
              />
            </div>
            <motion.div
              initial={{ opacity: 0, y: 40 }}
              animate={{ opacity: 1, y: 0, transition: { delay: 0.2 } }}
            >
              <Typography variant="h3" color="light" className="font-semibold leading-tight">
                APLIKASI <br /> INDEKS KERAWANAN
              </Typography>
            </motion.div>

            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1, transition: { delay: 0.3 } }}
            >
              <Typography className="mt-32 text-16  font-semibold logo-text" color="light">
                APILKASI INDEKS KERAWANAN APLIKASI YANG TERINTEGRASI UNTUK MENDAPATKAN INFORMASI DATA ORMAS YANG DIBUTUHKAN PIMPINAN
              </Typography>
            </motion.div>
          </div>
        </div>
      </motion.div>
    </Root>
  );
}

export default Login;
