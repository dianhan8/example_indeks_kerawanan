import { yupResolver } from "@hookform/resolvers/yup";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Icon from "@mui/material/Icon";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Typography from "@mui/material/Typography";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { submitLogin } from "app/auth/store/loginSlice";
import * as yup from "yup";
import _ from "@lodash";
import {
  Autocomplete,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import axios from "axios";
import { Box } from "@mui/system";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

import { injectStyle } from "react-toastify/dist/inject-style";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  email: yup
    .string()
    .email("You must enter a valid email")
    .required("You must enter a email"),
  password: yup
    .string()
    .required("Please enter your password.")
    .min(4, "Password is too short - should be 4 chars minimum."),
});

const defaultValues = {
  email: "",
  password: "",
};

function MabesLoginTab(props) {
  // console.log(props, "props");
  const dispatch = useDispatch();
  const login = useSelector(({ auth }) => auth.login);
  const {
    control,
    setValue,
    formState,
    handleSubmit,
    reset,
    trigger,
    setError,
  } = useForm({
    mode: "onChange",
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { isValid, dirtyFields, errors } = formState;

  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    setValue("email", "admin@sdataanalytics.com", {
      shouldDirty: true,
      shouldValidate: true,
    });
    setValue("password", "admin", { shouldDirty: true, shouldValidate: true });
  }, [reset, setValue, trigger]);

  useEffect(() => {
    login.errors.forEach((error) => {
      setError(error.type, {
        type: "manual",
        message: error.message,
      });
    });
  }, [login.errors, setError]);

  function onSubmit(model) {
    dispatch(submitLogin(model));
  }

  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loginFiled, setLoginFiled] = useState("");

  const handleUsername = (e) => {
    setUsername(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleTenant = (e) => {
    props.valueLoginMabes(e.target.value);
  };

  const navigate = useNavigate();

  if (typeof window !== "undefined") {
    injectStyle();
  }

  const handleSubmitMabes = (e) => {
    e.preventDefault();
    axios
      .post("http://medialisa.id:3080/login", {
        username: userName,
        password: password,
        tenant: props.valueLoginMabes.idDors.toString(),
      })
      .then((response) => {
        navigate("/apps/dashboards/analytics");
        console.log("cek respon login", response);
        if (response === 200) {
          toast.success("Data BERHASIL dihapus");
        }
      })
      .catch((error) => {
        setLoginFiled("Username atau Password Salah !!!");
        toast.error("Username atau Password Salah !!!",{
          pauseOnHover: false,
        });
        console.log(error, "err");
      });
  };

  // if (handleSubmitMabes.ok) {
  //     toast.success("Anda Berhasil Login", {
  //       position: "top-right",
  //       // autoClose: 3000,
  //       hideProgressBar: false,
  //       closeOnClick: true,
  //       pauseOnHover: true,
  //       draggable: true,
  //       progress: undefined
  //     });
  //   } else {
  //     toast.warn("some problem");
  //   }
  return (
    <div className="w-full">
      <form
        className="flex flex-col justify-center w-full"
        // onSubmit={handleSubmit(onSubmit)}
        onSubmit={handleSubmitMabes}
      >
        {/* <Autocomplete
          disablePortal
          id="combo-box-demo"
          getOptionLabel={(loginMabes) => loginMabes.name}
          options={props.loginMabes}
          noOptionsText={"Tidak ada yang di pilih"}
          value={props.valueLoginMabes}
          onChange={(event, newValue) => {
            props.setValueMabes(newValue);
            // console.log(newValue, "newvalueMabes")
          }}
          renderInput={(params) => (
            <TextField
              className="mb-16"
              variant="outlined"
              required
              value={props.valueLoginMabes}
              onChange={handleTenant}
              {...params}
              label="Data Analytics"
            />
          )}
        /> */}
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled === "" ? false : true}
            fullWidth
            labelId="username"
            id="standard-error-helper-text"
            label="Username"
            name="username"
            type="text"
            value={userName}
            onChange={handleUsername}
            // helperText={loginFiled}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled === "" ? false : true}
            fullWidth
            labelId="password"
            id="standard-error-helper-text"
            label="Password"
            name="password"
            type="password"
            value={password}
            onChange={handlePassword}
            helperText={loginFiled}
          />
        </FormControl>
        {/* <Controller
          name="NameMabes"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              label="Nama Mabes"
              error={!!errors.displayName}
              helperText={errors?.displayName?.message}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      person
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              required
            />
          )}
        /> */}

        {/* <Controller
          name="UserName"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              error={!!errors.email}
              helperText={errors?.email?.message}
              label="Username"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      user
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
            />
          )}
        /> */}

        {/* <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              label="Password"
              type="password"
              // error={!!errors.password}
              // helperText={errors?.password?.message}
              variant="outlined"
              // onChange={props.handleInput}
              InputProps={{
                className: "pr-2",
                type: showPassword ? "text" : "password",
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => setShowPassword(!showPassword)}
                      size="large"
                    >
                      <Icon className="text-20" color="action">
                        {showPassword ? "visibility" : "visibility_off"}
                      </Icon>
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              required
            />
          )}
        /> */}

        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="w-full mx-auto mt-16"
          aria-label="LOG IN"
          // disabled={_.isEmpty(dirtyFields) || !isValid}
          value="legacy"
        >
          Login
        </Button>
      </form>
      <ToastContainer  autoClose={2000} />
    </div>
  );
}

export default MabesLoginTab;
