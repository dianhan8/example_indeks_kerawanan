/* eslint-disable no-alert */
import { yupResolver } from '@hookform/resolvers/yup';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Icon from '@mui/material/Icon';
import InputAdornment from '@mui/material/InputAdornment';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { submitLogin } from 'app/auth/store/loginSlice';
import * as yup from 'yup';
import _ from '@lodash';
import { Autocomplete, FormControl } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  email: yup.string().email('You must enter a valid email').required('You must enter a email'),
  password: yup
    .string()
    .required('Please enter your password.')
    .min(4, 'Password is too short - should be 4 chars minimum.'),
});

const defaultValues = {
  email: '',
  password: '',
};

function PolsekLoginTab(props) {
  const dispatch = useDispatch();
  const login = useSelector(({ auth }) => auth.login);
  const { control, setValue, formState, handleSubmit, reset, trigger, setError } = useForm({
    mode: 'onChange',
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { isValid, dirtyFields, errors } = formState;

  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    setValue('email', 'admin@polsek.com', {
      shouldDirty: true,
      shouldValidate: true,
    });
    setValue('password', 'admin', { shouldDirty: true, shouldValidate: true });
  }, [reset, setValue, trigger]);

  useEffect(() => {
    login.errors.forEach((error) => {
      setError(error.type, {
        type: 'manual',
        message: error.message,
      });
    });
  }, [login.errors, setError]);

  function onSubmit(model) {
    dispatch(submitLogin(model));
  }
  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginFiled, setLoginFiled] = useState('');

  const handleUsername = (e) => {
    setUsername(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleTenant = (e) => {
    props.valueLoginPolsek(e.target.value);
  };

  const navigate = useNavigate();

  const handleSubmitPolsek = (e) => {
    e.preventDefault();
    axios
      .post('http://medialisa.id:3080/login', {
        username: userName,
        password,
        tenant: props.valueLoginPolsek.idDors.toString(),
      })
      .then((response) => {
        navigate('/apps/dashboards/analytics');
        console.log('cek respon login', response);
        alert('berhasil');

        if (response.status !== 200) {
          setLoginFiled('Username atau Password Salah');
        }
      })
      .catch((error) => {
        setLoginFiled('Username atau Password Salah !!!');
        console.log(error, 'err');
        alert('error');
      });
  };

  return (
    <div className="w-full">
      <form
        className="flex flex-col justify-center w-full"
        // onSubmit={handleSubmit(onSubmit)}
        onSubmit={handleSubmitPolsek}
      >
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          getOptionLabel={(loginPolres) => loginPolres.name}
          options={props.loginPolsek}
          value={props.valueLoginPolsek}
          onChange={(event, newValue) => {
            props.setValuePolsek(newValue);
            // console.log(newValue, "newvaluePolsek")
          }}
          noOptionsText="tidak ada yang di pilih"
          renderInput={(params) => (
            <TextField
              className="mb-16"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      person
                    </Icon>
                  </InputAdornment>
                ),
              }}
              {...params}
              variant="outlined"
              required
              label="Cari Polsek"
            />
          )}
        />
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled !== ''}
            fullWidth
            labelId="username"
            id="username"
            label="Username"
            name="username"
            type="text"
            value={userName}
            onChange={handleUsername}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled !== ''}
            fullWidth
            labelId="password"
            id="password"
            label="Password"
            name="password"
            type="text"
            value={password}
            onChange={handlePassword}
            helperText={loginFiled}
          />
        </FormControl>
        {/* <Controller
          name="NameMabes"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              label="Nama Polsek"
              error={!!errors.displayName}
              helperText={errors?.displayName?.message}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      person
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              required
            />
          )}
        /> */}
        {/* <Controller
          name="Username"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              error={!!errors.email}
              helperText={errors?.email?.message}
              label="Username"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      user
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
            />
          )}
        />

        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              label="Password"
              type="password"
              error={!!errors.password}
              helperText={errors?.password?.message}
              variant="outlined"
              InputProps={{
                className: "pr-2",
                type: showPassword ? "text" : "password",
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => setShowPassword(!showPassword)}
                      size="large"
                    >
                      <Icon className="text-20" color="action">
                        {showPassword ? "visibility" : "visibility_off"}
                      </Icon>
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              required
            />
          )}
        /> */}

        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="w-full mx-auto mt-16"
          aria-label="LOG IN"
          disabled={_.isEmpty(dirtyFields) || !isValid}
          value="legacy"
        >
          Login
        </Button>
      </form>

      <table className="w-full mt-32 text-center">
        <thead className="mb-4">
          <tr>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Role
              </Typography>
            </th>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Username
              </Typography>
            </th>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Password
              </Typography>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Typography className="font-medium text-11" color="textSecondary">
                Admin
              </Typography>
            </td>
            <td>
              <Typography className="text-11">polsekbaiturahman</Typography>
            </td>
            <td>
              <Typography className="text-11">12345678</Typography>
            </td>
          </tr>
          {/* <tr>
            <td>
              <Typography className="font-medium text-11" color="textSecondary">
                Staff
              </Typography>
            </td>
            <td>
              <Typography className="text-11">staff@polsek.com</Typography>
            </td>
            <td>
              <Typography className="text-11">staff</Typography>
            </td>
          </tr> */}
        </tbody>
      </table>
    </div>
  );
}

export default PolsekLoginTab;
