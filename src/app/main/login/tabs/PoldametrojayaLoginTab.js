import { yupResolver } from "@hookform/resolvers/yup";
import Button from "@mui/material/Button";
import Icon from "@mui/material/Icon";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Typography from "@mui/material/Typography";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { submitLoginWithFireBase } from "app/auth/store/loginSlice";
import * as yup from "yup";
import TextField from "@mui/material/TextField";
import _ from "@lodash";
import { Autocomplete, FormControl } from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  email: yup
    .string()
    .email("You must enter a valid email")
    .required("You must enter a email"),
  password: yup.string().required("Please enter your password."),
});

const defaultValues = {
  email: "",
  password: "",
};

function PoldametrojayaLoginTab(props) {
  // console.log(props, "poldametrojaya")
  const dispatch = useDispatch();
  const login = useSelector(({ auth }) => auth.login);
  const {
    control,
    setValue,
    formState,
    handleSubmit,
    reset,
    trigger,
    setError,
  } = useForm({
    mode: "onChange",
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { isValid, dirtyFields, errors } = formState;

  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    setValue("email", "admin@poldametrojaya.com", {
      shouldDirty: true,
      shouldValidate: true,
    });
    setValue("password", "admin", { shouldDirty: true, shouldValidate: true });
  }, [reset, setValue, trigger]);

  useEffect(() => {
    login.errors.forEach((error) => {
      setError(error.type, {
        type: "manual",
        message: error.message,
      });
    });
  }, [login.errors, setError]);

  function onSubmit(model) {
    dispatch(submitLogin(model));
  }

  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loginFiled, setLoginFiled] = useState("");

  const handleUsername = (e) => {
    setUsername(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleTenant = (e) => {
    props.valueLoginPolda(e.target.value);
  };

  const navigate = useNavigate();

  const handleSubmitPolda = (e) => {
    e.preventDefault();
    axios
      .post("http://medialisa.id:3080/login", {
        username: userName,
        password: password,
        tenant: props.valueLoginPolda.idDors.toString(),
      })
      .then((response) => {
        navigate("/apps/dashboards/analytics");
        console.log("cek respon login", response);
        alert("berhasil");

        if (response.status !== 200) {
          setLoginFiled("Username atau Password Salah");
        }
      })
      .catch((error) => {
        setLoginFiled("Username atau Password Salah !!!");
        console.log(error, "err");
        alert("error");
      });
  };

  return (
    <div className="w-full">
      <form
        className="flex flex-col justify-center w-full"
        onSubmit={handleSubmitPolda}
      >
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          getOptionLabel={(loginPolres) => loginPolres.name}
          options={props.loginPolda}
          noOptionsText={"tidak ada yang di pilih"}
          value={props.valueLoginPolda}
          onChange={(event, newValue) => {
            props.setValuePolda(newValue);
            // console.log(newValue, "newvaluePolda")
          }}
          renderInput={(params) => (
            <TextField
              className="mb-16"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      person
                    </Icon>
                  </InputAdornment>
                ),
              }}
              {...params}
              variant="outlined"
              required
              label="Cari Polda"
            />
          )}
        />
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled === "" ? false : true}
            fullWidth
            labelId="username"
            id="username"
            label="Username"
            name="username"
            type="text"
            value={userName}
            onChange={handleUsername}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            error={loginFiled === "" ? false : true}
            fullWidth
            labelId="password"
            id="password"
            label="Password"
            name="password"
            type="text"
            value={password}
            onChange={handlePassword}
            helperText={loginFiled}
          />
        </FormControl>

        {/* <Controller
          name="NameMabes"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              label="Nama Polda"
              error={!!errors.displayName}
              helperText={errors?.displayName?.message}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      person
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              required
            />
          )}
        /> */}
        {/* <Controller
          name="Username"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              type="text"
              error={!!errors.email}
              helperText={errors?.email?.message}
              label="Username"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Icon className="text-20" color="action">
                      user
                    </Icon>
                  </InputAdornment>
                ),
              }}
              variant="outlined"
            />
          )}
        />

        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              className="mb-16"
              label="Password"
              type="password"
              error={!!errors.password}
              helperText={errors?.password?.message}
              variant="outlined"
              InputProps={{
                className: 'pr-2',
                type: showPassword ? 'text' : 'password',
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={() => setShowPassword(!showPassword)} size="large">
                      <Icon className="text-20" color="action">
                        {showPassword ? 'visibility' : 'visibility_off'}
                      </Icon>
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              required
            />
          )}
        /> */}

        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="w-full mx-auto mt-16"
          aria-label="LOG IN"
          disabled={_.isEmpty(dirtyFields) || !isValid}
          value="legacy"
        >
          Login
        </Button>
      </form>

      <table className="w-full mt-32 text-center">
        <thead className="mb-4">
          <tr>
            <th>
              <Typography
                className="font-semibold text-11"
                color="textSecondary"
              >
                Role
              </Typography>
            </th>
            <th>
              <Typography
                className="font-semibold text-11"
                color="textSecondary"
              >
                Username
              </Typography>
            </th>
            <th>
              <Typography
                className="font-semibold text-11"
                color="textSecondary"
              >
                Password
              </Typography>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Typography className="font-medium text-11" color="textSecondary">
                Admin
              </Typography>
            </td>
            <td>
              <Typography className="text-11">poldaaceh</Typography>
            </td>
            <td>
              <Typography className="text-11">12345678</Typography>
            </td>
          </tr>
          {/* <tr>
            <td>
              <Typography className="font-medium text-11" color="textSecondary">
                Staff
              </Typography>
            </td>
            <td>
              <Typography className="text-11">
                staff@poldametrojaya.com
              </Typography>
            </td>
            <td>
              <Typography className="text-11">staff</Typography>
            </td>
          </tr> */}
        </tbody>
      </table>
    </div>
  );
}

export default PoldametrojayaLoginTab;
