import Card from "@mui/material/Card";
import { styled, darken } from "@mui/material/styles";
import CardContent from "@mui/material/CardContent";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import { motion } from "framer-motion";
import { useState } from "react";
import { Link } from "react-router-dom";
import Auth0RegisterTab from "./tabs/Auth0RegisterTab";
import FirebaseRegisterTab from "./tabs/FirebaseRegisterTab";
import JWTRegisterTab from "./tabs/JWTRegisterTab";
import MabesRegisterTab from "./tabs/MabesRegisterTab";
import PoldametrojayaRegisterTab from "./tabs/PoldametrojayaRegisterTab";
import PolresRegisterTab from "./tabs/PolresRegisterTab";
import PolsekRegisterTab from "./tabs/PolsekRegisterTab";

const Root = styled("div")(({ theme }) => ({
  background: `linear-gradient(to right, ${
    theme.palette.primary.dark
  } 0%, ${darken(theme.palette.primary.dark, 0.5)} 100%)`,
  color: theme.palette.primary.contrastText,

  "& .Register-leftSection": {},

  "& .Register-rightSection": {
    background: `linear-gradient(to right, ${
      theme.palette.primary.dark
    } 0%, ${darken(theme.palette.primary.dark, 0.5)} 100%)`,
    color: theme.palette.primary.contrastText,
  },
}));

function Register() {
  const [selectedTab, setSelectedTab] = useState(0);

  function handleTabChange(event, value) {
    setSelectedTab(value);
  }

  return (
    <Root className="flex flex-col flex-auto items-center justify-center shrink-0 p-16 md:p-24">
      <motion.div
        initial={{ opacity: 0, scale: 0.6 }}
        animate={{ opacity: 1, scale: 1 }}
        className="flex w-full max-w-400 md:max-w-3xl rounded-20 shadow-2xl overflow-hidden"
      >
        <Card
          className="Register-leftSection flex flex-col w-full max-w-sm items-center justify-center shadow-0"
          square
        >
          <CardContent className="flex flex-col items-center justify-center w-full max-w-320">
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1, transition: { delay: 0.2 } }}
            >
              <div className="flex items-center justif-center mb-10">
                <img
                  className="logo-icon w-48"
                  src="assets/images/avatars/logoPolriHeader.png"
                  alt="logo"
                />
                <div className="border-l-1 mr-4 w-1 h-40" />
                <div>
                  <Typography
                    className="text-24 font-semibold logo-text"
                    color="inherit"
                  >
                    BIG DATA POLRI
                  </Typography>
                  <Typography
                    className="text-16 tracking-widest -mt-8 font-700"
                    color="textSecondary"
                  >
                    MABES POLRI
                  </Typography>
                </div>
              </div>
            </motion.div>

            <Tabs
              value={selectedTab}
              onChange={handleTabChange}
              variant="fullWidth"
              className="w-full mb-32"
            >
              {/* <Tab
                icon={
                  <img
                    className="h-40 p-4 bg-black rounded-12"
                    src="assets/images/logos/jwt.svg"
                    alt="firebase"
                  />
                }
                className="min-w-0"
                label="JWT"
              /> */}
              <Tab
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="mabes"
                  />
                }
                className="min-w-0"
                label="MABES"
              />
              <Tab
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="mabes"
                  />
                }
                className="min-w-0"
                label="POLDA"
              />
              <Tab
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="mabes"
                  />
                }
                className="min-w-0"
                label="POLRES"
              />
              <Tab
                icon={
                  <img
                    className="h-40"
                    src="assets/images/avatars/logoPolriHeader.png"
                    alt="mabes"
                  />
                }
                className="min-w-0"
                label="POLSEK"
              />
            </Tabs>

            {selectedTab === 0 && <MabesRegisterTab />}
            {selectedTab === 1 && <PoldametrojayaRegisterTab />}
            {selectedTab === 2 && <PolresRegisterTab />}
            {selectedTab === 3 && <PolsekRegisterTab />}
            {/* {selectedTab === 0 && <JWTRegisterTab />}
            {selectedTab === 1 && <FirebaseRegisterTab />}
            {selectedTab === 2 && <Auth0RegisterTab />} */}
          </CardContent>

          <div className="flex flex-col items-center justify-center pb-10">
            <div>
              <span className="font-normal mr-8">Sudah Punya Akun?</span>
              <Link className="font-normal" to="/login">
                Login
              </Link>
            </div>
            <Link className="font-normal mt-8" to="/">
              Kembali Ke Dashboard
            </Link>
          </div>
        </Card>

        <div className="Login-rightSection hidden md:flex flex-1 items-center justify-center p-64">
          <div className="max-w-800 text-center">
            <div className="flex justify-center">
              <img
                className="flex items-center logo-icon w-192"
                src="assets/images/avatars/logoPolriHeader.png"
                alt="logo"
              />
            </div>
            <motion.div
              initial={{ opacity: 0, y: 40 }}
              animate={{ opacity: 1, y: 0, transition: { delay: 0.2 } }}
            >
              <Typography
                variant="h3"
                color="inherit"
                className="font-semibold leading-tight"
              >
                SELAMAT DATANG APLIKASI <br /> BIG DATA POLRI
              </Typography>
            </motion.div>

            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1, transition: { delay: 0.3 } }}
            >
              <Typography variant="subtitle1" color="inherit" className="mt-32">
                APILKASI INI MENYAJIKAN DATA ANALISIS YANG TERINTEGRASI DARI
                BEBERAPA APLIKASI DILINGKUNGAN POLRI, DIHARAPKAN DAPAT
                BERMANFAAT BAGI UNSUR PIMPINAN DALAM PENGAMBILAN KEPUTUSAN
                PENGAMBILAN KEPUTUSAN LEBIH LANJUT
              </Typography>
            </motion.div>
          </div>
        </div>
      </motion.div>
    </Root>
  );
}

export default Register;
