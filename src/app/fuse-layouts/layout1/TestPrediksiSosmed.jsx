import * as React from 'react';
import Box from '@mui/material/Box';
import SpeedDial from '@mui/material/SpeedDial';
import SpeedDialIcon from '@mui/material/SpeedDialIcon';
import SpeedDialAction from '@mui/material/SpeedDialAction';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import { makeStyles } from '@mui/styles';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
// import { makeStyles } from '@mui/material';

const actions = [
  { icon: <FacebookIcon fontSize="large" color="primary" />, name: 'Facebook', bgColor: '#08C2FF' },
  {
    icon: <InstagramIcon fontSize="large" color="primary" />,
    name: 'Instagram',
    bgColor: '#08C2FF',
  },
  { icon: <TwitterIcon fontSize="large" color="primary" />, name: 'Twitter', bgColor: '#08C2FF' },
  { icon: <YouTubeIcon fontSize="large" color="primary" />, name: 'Youtube', bgColor: '#08C2FF' },
];
const useStyles = makeStyles((theme) => ({
  tooltip: {
    marginTop: '-.01rem',
    fontSize: '5rem',
  },
}));

export default function TestPrediksiSosmed() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <Box sx={{ height: 0, transform: 'translateZ(0px)', flexGrow: 1 }}>
      <SpeedDial
        ariaLabel="SpeedDial openIcon example"
        sx={{ bottom: 16, right: 16 }}
        icon={<SpeedDialIcon openIcon={<NotificationsActiveIcon />} />}
        direction="right"
      >
        {actions.map((action) => (
          <SpeedDialAction key={action.name} icon={action.icon} tooltipTitle={action.name} />
        ))}
      </SpeedDial>
    </Box>
  );
}
