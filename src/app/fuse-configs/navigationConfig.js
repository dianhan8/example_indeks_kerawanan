import i18next from 'i18next';
// import DocumentationNavigation from "../main/documentation/DocumentationNavigation";

import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
  {
    id: 'applications',
    title: 'Applications',
    translate: 'APPLICATIONS',
    type: 'group',
    icon: 'apps',
    children: [
      {
        id: 'dashboards',
        title: 'Overview Wilayah',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/dashboards/analytics',
        children: [
          {
            id: 'analytics-dashboard',
            title: 'Overview Wilayah',
            type: 'item',
            url: 'apps/dashboards/analytics',
          },
          // {
          //   id: 'project-dashboard',
          //   title: 'Anev Perjenis Kejahatan',
          //   type: 'item',
          //   url: 'apps/dashboards/project',
          // },
        ],
      },
      {
        id: 'Profiling',
        title: 'Profiling Ormas',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/ProfilingOrmas/analytics',
        children: [
          {
            id: 'profiling-dashboard',
            title: 'Analytics',
            type: 'item',
            url: 'apps/ProfilingOrmas/analytics',
          },
        ],
      },
      {
        id: 'Report',
        title: 'Report',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/Report/analytics',
        children: [
          {
            id: 'Report-dashboard',
            title: 'Analytics',
            type: 'item',
            url: 'apps/Report/analytics',
          },
        ],
      },
      {
        id: 'Analitik',
        title: 'Analitik',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/Analitik/analytics',
        children: [
          {
            id: 'Analitik-dashboard',
            title: 'Analytics',
            type: 'item',
            url: 'apps/Analitik/analytics',
          },
        ],
      },
      {
        id: 'SumberData',
        title: 'Sumber Data',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/SumberData/analytics',
        children: [
          {
            id: 'SumberData-dashboard',
            title: 'Analytics',
            type: 'item',
            url: 'apps/SumberData/analytics',
          },
        ],
      },
      {
        id: 'UserManagement',
        title: 'User Management',
        type: 'item',
        icon: 'dashboard',
        url: 'apps/UserManagement/analytics',
        children: [
          {
            id: 'UserManagement-dashboard',
            title: 'Analytics',
            type: 'item',
            url: 'apps/UserManagement/analytics',
          },
        ],
      },
      // {
      //   id: 'Report',
      //   title: 'Report',
      //   type: 'item',
      //   icon: 'dashboard',
      //   url: 'apps/dashboards/profiling',
      //   children: [
      //     {
      //       id: 'report-dashboard',
      //       title: 'Analytics',
      //       type: 'item',
      //       url: 'apps/dashboards/profiling',
      //     },
      //   ],
      // },
      // {
      //   id: '1',
      //   title: 'Profiling Ormas',
      //   type: 'collapse',
      //   icon: 'pages',
      //   url: 'apps/kejahatan/dataKejahatanPage',
      // },
      // {
      //   id: 'laluLintas',
      //   title: 'Report',
      //   type: 'collapse',
      //   icon: 'pages',
      //   url: 'apps/lalulintas/KejadianLakaLantasPage',
      // },
      // {
      //   id: 'tahanan',
      //   title: 'Analitik',
      //   type: 'collapse',
      //   icon: 'verified_user',
      //   url: 'apps/tahanan/tahanNegaraPage',
      // },
      // {
      //   id: 'pages',
      //   title: 'Sumber Data',
      //   type: 'collapse',
      //   icon: 'pages',
      //   url: 'apps/karhutla/karhutlaAlamPages',
      // },
      // {
      //   id: 'pages',
      //   title: 'User Management',
      //   type: 'collapse',
      //   icon: 'pages',
      //   url: 'apps/covid19/dataCovidPages',
      // },
    ],
  },
];

export default navigationConfig;
