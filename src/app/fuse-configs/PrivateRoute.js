import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { routeConfigs } from "./routesConfig";

export const PrivateRoute = () => {
  if (routeConfigs) {
    return <Outlet />;
  } else {
    return <Navigate to="/login" />;
  }
};
